    pv_cnHeightFoto       NUMBER(10,3) := 0.5;

    pv_cnHeightRest       NUMBER(10,3) := 1.764;

    pv_baffoHeight        NUMBER(10,3) := 2.469;

   

    pv_moduleCounter      NUMBER(10,3) := 0.0;

    pv_heightCounter      NUMBER(10,3) := 0.0;

 

    pv_heightFixedElement NUMBER(10,3) := 0.0;

   

    pv_wordCounter    NUMBER(10,3) := 0;

   

    pv_charCounter    NUMBER(10,3) := 0;

   

    pv_currentTag     VARCHAR2(4000) := '';

    pv_previousTag    VARCHAR2(4000) := '';

    pv_height         NUMBER(8,2):= 0 ;  

    pv_width          NUMBER(8,2):= 0 ;

    pv_grl_cuneo_necro NUMBER(8,2) := 0 ;
   
    FUNCTION FP_refreshModuleCounter ( i_testo    IN VARCHAR2     , 
                                       i_tag      IN rec_marcatore, 
                                       i_mrc_prec IN VARCHAR2      ) 
      -- (_pos:int, _tag:String, _onlyText:String) 
      RETURN NUMBER IS 
      lv_count_module  NUMBER(10,2) := 0;
      lv_cn            NUMBER := 0;
      lv_currentTag    VARCHAR2(5) ; 
      lv_previousTag   VARCHAR2(5) ; 
      lv_charsLength   NUMBER(10,2)  := 0 ; 
      lv_line          NUMBER := 0 ; 
      lv_firstline     NUMBER := 0 ;  
      lv_lineCounter   NUMBER := 0 ; 
      lv_heightCounter NUMBER := 0 ; 
      lv_tempHeightCounter NUMBER  := 0 ;  
      lv_pesoparole        NUMBER := 0 ;  
      LV_ERRORE VARCHAR2(2000) ; 
    BEGIN  
      lv_currentTag := i_tag.mrc_id ; 
      lv_previousTag := i_mrc_prec ;
      IF ( 'CO' = lv_previousTag AND 'ON' = lv_currentTag ) THEN
        lv_cn := pv_baffoHeight;
       ELSIF ( 'FOTO' = lv_previousTag AND 'NOME' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightFoto;
       ELSIF ( 'NOME' = lv_previousTag AND 'CO' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightRest;
       ELSIF ( 'NOME' = lv_previousTag AND 'T' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightRest;
       ELSIF ( 'NOME' = lv_previousTag AND 'ON' = lv_currentTag ) THEN
        lv_cn := pv_baffoHeight;
       ELSIF ( 'SNO' = lv_previousTag AND 'T' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightFoto;
       ELSIF ( 'SNO' = lv_previousTag AND 'CO' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightRest;
       ELSIF ( 'T' = lv_previousTag AND 'ON' = lv_currentTag ) THEN
        lv_cn := pv_baffoHeight;
       ELSIF ( 'T' = lv_previousTag AND 'TT' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightRest;
       ELSIF ( 'T' = lv_previousTag AND 'NOME' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightRest;
       ELSIF ( 'T' = lv_previousTag AND 'FOTO' = lv_currentTag ) THEN
        lv_cn := pv_cnHeightFoto;
       ELSIF ( 'TN' = lv_previousTag AND 'ON' = lv_currentTag ) THEN
        lv_cn := pv_baffoHeight;
       END IF ; 
                          
      IF ( i_tag.mrc_mrca_tp IN ( 'T' , 'N' , 'C' ) ) THEN       -- Sull'editor la croce è considerata come un testo

        lv_charsLength := NVL( length( i_testo), 0 ) ;
        lv_charsLength := lv_charsLength * i_tag.stlc_char_size ;
        lv_pesoparole  := lv_charsLength;
        lv_line := pv_width - i_tag.Stlc_right_Indent - i_tag.stlc_left_Indent;
        lv_firstLine := lv_line - i_tag.stlc_first_Line;
        
        lv_lineCounter := 0;

        WHILE (true) 
        LOOP 
          IF (  lv_charsLength  < 0 ) THEN EXIT;
          END IF ; 
          
          IF ( 0 = lv_lineCounter ) THEN lv_charsLength := lv_charsLength - lv_firstLine;
          ELSE lv_charsLength := lv_charsLength - lv_line;
          END IF ;
          lv_lineCounter := lv_lineCounter + 1 ;

          
        END LOOP ;
        
        lv_tempHeightCounter := i_tag.stlc_space_Before;
        lv_tempHeightCounter := lv_tempHeightCounter + i_tag.stlc_space_After;
        lv_tempHeightCounter := lv_tempHeightCounter + (i_tag.stlc_leading * lv_lineCounter);      

      ELSE  
        lv_tempHeightCounter := i_tag.stlc_space_Before;
        lv_tempHeightCounter := lv_tempHeightCounter + i_tag.stlc_space_After;
        lv_tempHeightCounter := lv_tempHeightCounter + i_tag.stlc_height;     
      END IF; 


      pv_heightCounter := pv_heightCounter + lv_tempHeightCounter;
      
      pv_heightCounter := pv_heightCounter + lv_cn;
      
      pv_moduleCounter := 0;
      IF ( pv_heightCounter <= pv_height ) THEN 
        pv_moduleCounter := 1;
      ELSE
        lv_tempHeightCounter := pv_heightCounter;
        while (TRUE) 
        LOOP 
          if ( lv_tempHeightCounter <= pv_height ) THEN 
            if ( lv_tempHeightCounter >= 2 /*( tempHeightCounter > 1 )*/ ) THEN 
              pv_moduleCounter := pv_moduleCounter + 0.5; 
              lv_tempHeightCounter := lv_tempHeightCounter - (pv_height / 2);
              if ( lv_tempHeightCounter >= 2 ) /*( tempHeightCounter > 1 )*/ THEN 
               pv_moduleCounter := pv_moduleCounter +0.5;
              END IF;
            END IF ; 
            EXIT;
          END IF;
          lv_tempHeightCounter := lv_tempHeightCounter - pv_height;
          pv_moduleCounter:= pv_moduleCounter + 1;
        END LOOP ;
      END IF;
      
      pv_heightCounter := trunc((pv_heightCounter)*10)/10;
      
      RETURN pv_moduleCounter;
    END ; 
 
   FUNCTION FP_CONTAMODULI ( i_ann_eserc_id        IN VARCHAR2,
                             i_ann_soc_id          IN VARCHAR2,
                             i_ann_uop_id          IN VARCHAR2,
                             i_ann_fil_id          IN VARCHAR2,
                             i_ann_nmg_id          IN VARCHAR2,
                             i_ann_id              IN NUMBER  , 
                             i_ann_tst_id          IN Varchar2 ) RETURN NUMBER IS
     lv_tag rec_marcatore ; 
     lv_mrc_prec          tmarcatori.mrc_mrca_id%TYPE := NULL ; 
     lv_moduli            NUMBER :=0 ; 
     lv_pos               NUMBER :=0 ;
     lv_testo             tanntesto.annt_testo%TYPE;
     lv_testo_help        tanntesto.annt_testo%TYPE;
     
   BEGIN 
     OPEN c_griglie (i_ann_tst_id) ; 
     FETCH c_griglie INTO pv_width , pv_height , pv_grl_cuneo_necro ; 
     CLOSE C_GRIGLIE ; 
     pv_moduleCounter := 0 ; 
     pv_width         := pv_width / 100 ;
     pv_height        := pv_height ;
     pv_heightCounter := nvl( pv_grl_cuneo_necro,0 ) /10 ; 

     FOR rec IN ( SELECT annt_testo         ,annt_mrc_tst_id    , p.annt_mrc_edz_id, p.annt_mrc_id      , 
                         mrc_mrca_tp        ,stlc_id            ,stlc_leading       ,
                         stlc_space_before  ,stlc_space_after   ,stlc_right_indent  ,
                         stlc_left_indent   ,stlc_first_line    ,stlc_char_size     ,
                         stlc_height  
                    FROM tmarcatori m , tstilicaratteri c, tanntesto p
                   WHERE p.annt_ann_eserc_id = i_ann_eserc_id
                     AND P.ANNT_ANN_SOC_ID   = i_ann_soc_id
                     AND p.annt_ann_uop_id   = i_ann_uop_id
                     AND P.ANNT_ANN_fil_ID   = i_ann_fil_id
                     AND p.annt_ann_nmg_id   = i_ann_nmg_id
                     AND p.annt_ann_id       = i_ann_id
                     AND m.mrc_tst_id        = annt_mrc_tst_id
                     AND m.mrc_edz_id        = annt_mrc_edz_id
                     AND m.mrc_rbra_seq_id   = annt_mrc_rbra_seq_id 
                     AND m.mrc_mrca_id       = annt_mrc_id 
                     AND c.stlc_id           = m.mrc_stlc_id
                    ORDER BY p.annt_ord )
     LOOP 
       lv_tag.mrc_id              := rec.annt_mrc_id       ;
       lv_tag.mrc_mrca_tp         := rec.mrc_mrca_tp       ;
       lv_tag.stlc_id             := rec.stlc_id           ;
       lv_tag.stlc_leading        := rec.stlc_leading      ;
       lv_tag.stlc_space_before   := rec.stlc_space_before ;
       lv_tag.stlc_space_after    := rec.stlc_space_after  ;
       lv_tag.stlc_right_indent   := rec.stlc_right_indent ;
       lv_tag.stlc_left_indent    := rec.stlc_left_indent  ;
       lv_tag.stlc_first_line     := rec.stlc_first_line   ;
       lv_tag.stlc_char_size      := rec.stlc_char_size    ;
       lv_tag.stlc_height         := rec.stlc_height       ;   
       
       lv_pos := INSTR ( rec.annt_testo , chr(10)) ; 
       
       IF lv_pos > 0 THEN 
         lv_testo_help := rec.annt_testo ; 
         LOOP 
           IF lv_testo_help IS NULL THEN EXIT ; 
           ELSE 
             IF lv_pos = 0 THEN 
               lv_testo := lv_testo_help ; 
               lv_testo_help := NULL ; 
             ELSE 
               lv_testo := substr( lv_testo_help , 1,lv_pos-1 ) ;
               lv_testo_help := substr( lv_testo_help , lv_pos + 1 ) ;
               lv_pos := INSTR ( lv_testo_help , chr(10)) ; 
             END IF ;

             lv_moduli := FP_refreshModuleCounter ( i_testo    => trim(lv_testo)     , 
                                                    i_tag      => lv_tag        , 
                                                    i_mrc_prec => lv_mrc_prec  ) ;
             lv_mrc_prec := rec.annt_mrc_id       ;
           END IF ; 
         END LOOP ; 
       ELSE 
         lv_moduli := FP_refreshModuleCounter ( i_testo    => trim(rec.annt_testo)    , 
                                                i_tag      => lv_tag             , 
                                                i_mrc_prec => lv_mrc_prec          ) ;
       END IF ;                                
       lv_mrc_prec := rec.annt_mrc_id       ;
     END LOOP ;    
   
     RETURN pv_moduleCounter ;             
   END ;
 
