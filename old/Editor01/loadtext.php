<?php
// Modifiche
// 15.01.2009 - Gigi - Linee 156 e segg; 274 e segg: visualizzati i marcatori dei riferimenti solo se flag_rif = 1
// 14.05.2009 - Gigi - Linea 403: mancava parentesi graffa aperta dopo if ($tag_info['tipo']!='F'); la parentesi si chiude prima di quella di foreach($tags as $tag_id => $tag_info) attualmente alla linea 425
// 05.08.2010 - Gigi - Aggiunta gestione del testo specifico per testata (TANNTESTOSPCF)
// 24.08.2010 - Gigi - Adeguamento ai multiriferimenti
// 08.09.2010 - Gigi - Elenco dei marcatori dei riferimenti utilizzabili ma non utilizzati (<tagrifs>), separato da quelli utilizzati
// 13.09.2010 - Gigi - Linea 374: aggiunta else if ( $qs['TIPOEDITOR']=='CEA' ) per il testo aggiuntivo; aggiunta condizione su mrc_id_rif nella 2^ select  union
// 06.11.2010 - Gigi - Terminata gestione tipo editor CEA - testi aggiuntivi; da verificare gestione base64_encode
// 24.06.2011 - Gigi - Introdotto ConvertCharset per risolvere il problema dell'euro (linea 852); gestito il caso in cui nelle foto non ci sia mrc_testo (veniva messo uno spazio in
//                     piu' che non faceva piu' riconoscere le foto ai tipi editor CES e CEA - linea 601)
// 06.07.2011 - Gigi - Problema con le foto "specifiche": aggiunto "AND mrc_flag_spcf   = 0" a riga 312 e 339 se tipo_editor=CEA; $fotoannunci (riga 592 ss): caricati su array
//                     solo le foto "specifiche" se CEA e NON specifiche se editor di altro tipo; aggiunto elemento flagspcf negli array $tags, $tagaggs, $tagaggrifs (riga 369 ss)
// 02.01.2012 - Gigi - Introdotto 14esimo parametro - FLAGFOTOOBBL - per rendere obbligatoria la foto dove previsto; gestione <tagaggs> (linea 877 ss): corretto baco che non faceva visualizzare
//                     le descrizioni di tutti i tipi di riferimento presenti
// 12.05.2015 - Gigi - Aggiunta gestione tipo marcatore "N" = nome defunto
// 18.05.2015 - Gigi - Gestione Riquadrati
// 19.05.2015 - Steve - Introdotte la lettura delle misure delle dimensioni dei moduli (larghezza x altezza), misure in millimetri
// 21.05.2015 - Steve -  Introdotta la lettura della dimensione fissa da utilizzare per il calcolo dei moduli nel necrologio
// 21.05.2015 - Steve -  Introdotta la lettura dei dati della tabella TSTILICARATTERI per effettuare il calcolo dei necrologi
// 14.07.2015 - Gigi - Tolta scrittura id annuncio nel cassetto dell'impresa
// 14.10.2015 - Gigi - Parametrizzato charset di conversione in $editor_config['CHARSET'], settato in ./include/config.inc.php
// 2015.12.18 - Gigi - Aggiunto ovunque controllo su mrc_dt_annl
// 2016.01.28 - Steve & Gigi - Eliminate le accentate nel tag FOTO

// linea 573 edito willbiteditor t1.insertstring
// modificato DinoTextArea.as riga 519 ss


	require('./include/db_connect.php');
	require_once "./ConvertCharset.class.php";
	
	$sourceCharset = "";
	
	if ( $editor_config['CHARSET'] == null OR $editor_config['CHARSET'] == "" ){
   	    $sourceCharset = "iso-8859-15";
	} else{
	    $sourceCharset = $editor_config['CHARSET'];
	}
	
	//$NewEncoding = new ConvertCharset("iso-8859-15", "utf-8", 0);
	$NewEncoding = new ConvertCharset($sourceCharset, "utf-8", 0);
    
	// per prima cosa faccio un test se ID mi viene correttamente passato
	// in caso contrario genero una response di errore
	$errore = 0;
	$errore_descrizione = "";

	$id = "";
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}

	if ($id == "") {
		$errore = 10000;
		$errore_descrizione = htmlspecialchars("TEXT ID IS UNDEFINED\nThe parent html page does not have and pass parameter: ID");
	}
	
	$query = explode("|", $id);
	
	$contaElem = count($query);
	
	if ( $contaElem < 13 )	{
		$errore = 10001;
		$errore_descrizione = htmlspecialchars("WRONG PARAMETERS NUMBER\nYou pass id=".$id);
	} else{
		if ( $contaElem == 13 )	{
			// Solo per compatibilita' con la versione a 13 parametri, senza flag_foto_obbl
			$qs = array (
				'ESERC' 	 => $query[0],
				'SOC' 	     => $query[1],
				'UOP' 	 	 => $query[2],
				'FIL' 	     => $query[3],
				'NUM' 	     => $query[4],
				'ID' 	  	 => $query[5],
				'UTENTE'     => $query[6],
				'TST'        => $query[7],
				'EDZ' 	     => $query[8],
				'RUBRSEQ'    => $query[9],
				'ATIPA'      => $query[10],
				'IMPRESA'    => $query[11],
				'TIPOEDITOR' => $query[12]
			);
		} elseif ( $contaElem == 14 ){
			$qs = array (
				'ESERC' 	   => $query[0],
				'SOC' 	       => $query[1],
				'UOP' 	 	   => $query[2],
				'FIL' 	       => $query[3],
				'NUM' 	       => $query[4],
				'ID' 	  	   => $query[5],
				'UTENTE'       => $query[6],
				'TST'          => $query[7],
				'EDZ' 	       => $query[8],
				'RUBRSEQ'      => $query[9],
				'ATIPA'        => $query[10],
				'IMPRESA'      => $query[11],
				'TIPOEDITOR'   => $query[12],
				'FLAGFOTOOBBL' => $query[13]
			);
			$flagfotoobbl = $qs['FLAGFOTOOBBL'];
		} else { // 19 elementi - necro tabellari
			$qs = array (
				'ESERC'  	    => $query[0],
				'SOC' 	        => $query[1],
				'UOP' 	 	    => $query[2],
				'FIL' 	        => $query[3],
				'NUM' 	        => $query[4],
				'ID' 	  	    => $query[5],
				'UTENTE'        => $query[6],
				'TST'           => $query[7],
				'EDZ' 	        => $query[8],
				'RUBRSEQ'       => $query[9],
				'ATIPA'         => $query[10],
				'IMPRESA'       => $query[11],
				'TIPOEDITOR'    => $query[12],
				'FLAGFOTOOBBL'  => $query[13],
				'FLAGTABELLARE' => $query[14],
				'MODLARGHEZZA'  => $query[15],
				'MODALTEZZA'    => $query[16],
				'ALTEZZAFIX'    => $query[17],
				'MODULI'		=> $query[18]
			);
			$flagTabellare = $qs['FLAGTABELLARE'];
		}

$fp_log=fopen("LOG_LOADTEXT".".txt",'a+');
fwrite($fp_log, date("Y-m-d G:i:s")." - count 19 ".$id.chr(13).chr(10) );
foreach($qs as $k => $v) {
  fwrite($fp_log, date("Y-m-d G:i:s")." - key ".$k." value ".$v.chr(13).chr(10));
}
fclose($fp_log);
    
		$ute_id = ""; $ute_descriz = ""; $flag_apri="";
		$sql = "BEGIN  PCK_ANNUNCI.PP_CHK_ALCK(".
				"'".$qs['ESERC']."',".
				"'".$qs['SOC']."',".
				"'".$qs['UOP']."',".
				"'".$qs['FIL']."',".
				"'".$qs['NUM']."',".
				"'".$qs['ID']."',".
				":o_flag_apri, :o_ute_id, :o_ute_descriz); END;";
		$params = array(
	        ":o_flag_apri"		=> array(&$flag_apri, 10, null),
	        ":o_ute_id"			=> array(&$ute_id, 50, null),
	        ":o_ute_descriz"	=> array(&$ute_descriz, 50, null)
		);
		$DB->db_query($sql, $params);
		
		if ($flag_apri=="FALSE") 
		{
			$errore = 10002;
			$errore_descrizione = htmlspecialchars("ANNUNCIO GIA' APERTO DA:\nUserID: ".$ute_id."\nDescrizione: ".$ute_descriz);
		}
		else if ($flag_apri=='NOT FOUND')
		{
		//	$errore = 10003;
		//	$errore_descrizione = htmlspecialchars("L'ANNUNCIO DEVE ESSERE APERTO DA ONE");
		}
  
	} 
	
	if ($errore)
	{
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
?>
<willbiteditor>
	<error number="<?php echo $errore;?>" description="<?php echo $errore_descrizione;?>" />
</willbiteditor>
<?php
		exit();
	}

  // Variabili che contengono i caratteri che delimitano i marcatori
  $openMrc  = "{";
  $closeMrc = "}";
  
	
	$sql = "SELECT ann_atipa_id    TIPA, /* Tipologia annuncio */
				   ANNR_MRC_ID_RIF MRC_RIF, /* Marcatore primo rif */
				   ANNR_FLAG_RIF   FLAG_RIF, /* Flag primo rif */
				   annt_mrc_id     MRC, /*codice marcatore */
				   annt_testo      TESTO , /*testo annuncio */
				   annt_dfnt_id    DEFUNTO_ID, /*codice defunto con foto */
				   annt_fte_id     FOTO_ID, /*codice foto per i fotoannunci economici */
				   annt_mrc_id_riq_iniz RIQ_INIZ, /*codice inizio riquadro */
				   annt_mrc_id_riq_fine,  /*codice fine riquadro */
				   annt_ord        ORD   ,
				   mrc_mrca_tp     MRC_TIPO,
				   mrc_flag_spcf   ,
				   'X'             FLAG_PD
	         FROM TMARCATORI, TANNTESTO, TANNUNCIRIF, TANNUNCI
			   WHERE ann_eserc_id      = :param1
			     AND ann_soc_id        = :param2
			     AND ann_uop_id        = :param3
			     AND ann_fil_id        = :param4
			     AND ann_nmg_id        = :param5
			     AND ann_id            = :param6
	             AND annt_ann_eserc_id (+)= ann_eserc_id
	             AND annt_ann_soc_id   (+)= ann_soc_id
	             AND annt_ann_uop_id   (+)= ann_uop_id
	             AND annt_ann_fil_id   (+)= ann_fil_id
	             AND annt_ann_nmg_id   (+)= ann_nmg_id
	             AND annt_ann_id       (+)= ann_id
	             AND annr_ann_eserc_id (+)= annt_ann_eserc_id
	             AND annr_ann_soc_id   (+)= annt_ann_soc_id
	             AND annr_ann_uop_id   (+)= annt_ann_uop_id
	             AND annr_ann_fil_id   (+)= annt_ann_fil_id
	             AND annr_ann_nmg_id   (+)= annt_ann_nmg_id
	             AND annr_ann_id       (+)= annt_ann_id
	             AND annr_mrc_id_rif   (+)= annt_mrc_id
				 AND annr_flag_rif     (+)= 1
				 /* AND NVL( annt_flag_mrc_rif, 0) = 0 */
			     AND mrc_tst_id        (+)= annt_mrc_tst_id
				 AND mrc_edz_id        (+)= annt_mrc_edz_id
				 AND mrc_rbra_seq_id   (+)= annt_mrc_rbra_seq_id
				 AND mrc_mrca_id       (+)= annt_mrc_id
		UNION
			SELECT ann_atipa_id     TIPA, /* Tipologia annuncio */
				   ANNR_MRC_ID_RIF  MRC_RIF, /* Marcatore primo rif */
				   ANNR_FLAG_RIF    FLAG_RIF, /* Flag primo rif */
		           annts_mrc_id     MRC, /*codice marcatore */
		           annts_testo      TESTO , /*testo annuncio */
		           annts_dfnt_id    DEFUNTO_ID, /*codice defunto con foto */
		           annts_fte_id     FOTO_ID, /*codice foto per i fotoannunci economici */
		           annts_mrc_id_riq_iniz RIQ_INIZ, /*codice inizio riquadro */
		           annts_mrc_id_riq_fine,  /*codice fine riquadro */
		           annts_ord        ORD   ,
		           mrc_mrca_tp      MRC_TIPO,
				   mrc_flag_spcf    ,
				   annts_flag_pd    FLAG_PD
		        FROM TMARCATORI, TANNTESTOSPCF, TANNUNCIRIF, TANNUNCI
			   WHERE ann_eserc_id      = :param1
			     AND ann_soc_id        = :param2
			     AND ann_uop_id        = :param3
			     AND ann_fil_id        = :param4
			     AND ann_nmg_id        = :param5
			     AND ann_id            = :param6
	             AND annr_ann_eserc_id  (+)= annts_ann_eserc_id
	             AND annr_ann_soc_id    (+)= annts_ann_soc_id
	             AND annr_ann_uop_id    (+)= annts_ann_uop_id
	             AND annr_ann_fil_id    (+)= annts_ann_fil_id
	             AND annr_ann_nmg_id    (+)= annts_ann_nmg_id
	             AND annr_ann_id        (+)= annts_ann_id
	             AND annr_mrc_id_rif    (+)= annts_mrc_id
				 AND annr_flag_rif      (+)= 1
		         AND annts_ann_eserc_id (+)= ann_eserc_id
		         AND annts_ann_soc_id   (+)= ann_soc_id
		         AND annts_ann_uop_id   (+)= ann_uop_id
		         AND annts_ann_fil_id   (+)= ann_fil_id
		         AND annts_ann_nmg_id   (+)= ann_nmg_id
				 AND annts_mrc_tst_id   (+)= :param7
				 AND annts_mrc_edz_id   (+)= :param8
		         AND annts_ann_id       (+)= ann_id
		         AND mrc_tst_id         (+)= annts_mrc_tst_id
			     AND mrc_edz_id         (+)= annts_mrc_edz_id
			     AND mrc_rbra_seq_id    (+)= annts_mrc_rbra_seq_id
			     AND mrc_mrca_id        (+)= annts_mrc_id
		   ORDER BY ORD ";

	$params = array(
		':param1' => $qs['ESERC'],
		':param2' => $qs['SOC'],
		':param3' => $qs['UOP'],
		':param4' => $qs['FIL'],
		':param5' => $qs['NUM'],
		':param6' => $qs['ID'],
		':param7' => $qs['TST'],
		':param8' => $qs['EDZ'],
	);
	$DB->db_query($sql, $params);

	$data = "";
	$rifs = array();
	$indice = 0;
	while($result = $DB->db_fetch_array()) 
	{
		// I marcatori di tipo "foto" hanno trattamento particolare
		if ($result['MRC_TIPO']=='F') {
			if ($qs['TIPOEDITOR']=='CN') {
				$data .= $openMrc.$result['MRC']." | ".$result['DEFUNTO_ID']." | ".$result['TESTO'].$closeMrc."\n";
			}
			else if ($qs['TIPOEDITOR']=='CEA') {
				$data .= $openMrc.$result['MRC']." | ".$result['FOTO_ID']." | ".$result['TESTO'].$closeMrc."<---".$result['FLAG_PD']."\n";
			}
			else {
				$data .= $openMrc.$result['MRC']." | ".$result['FOTO_ID']." | ".$result['TESTO'].$closeMrc."\n";
			}
		}
		else {
//echo "ci sono - ".$result['TESTO']."<br>";
			if ( !empty($result['MRC']) ) {
				// Se tipo editor = CEA, al fondo aggiungo l'indicazione se il testo va all'inizio o alla fine
				if ($qs['TIPOEDITOR']=='CEA') {
					// Se il flag Prima/Dopo e' significativo, lo aggiungo al fondo del testo
					if ( $result['FLAG_PD'] == 'P' or $result['FLAG_PD']== 'D') {
						$data .= $openMrc.$result['MRC'].$closeMrc.$result['TESTO']."<---".$result['FLAG_PD'];
					}
					else{
						$data .= $openMrc.$result['MRC'].$closeMrc.$result['TESTO'];
					}
				}
				else{
   						$data .= $openMrc.$result['MRC'].$closeMrc.$result['TESTO'];
				}
			}
		}
//		if ($DB->rec_position==0) {
		if ($indice == 0) {
			$riqIniz = $result['RIQ_INIZ'];
		}
		$indice++;
		
		// Tipologia testo
		$tipa = $result['TIPA'];
		
		
		$rifs[] = array(
				'MRC_RIF'  => $result[1],
				'FLAG_RIF' => $result[2],
				'MRC'      => $result[3]
			);
			
	}
//var_dump($data);
	// L'editor e' di tipo complesso
	if ($qs['TIPOEDITOR']=='CN' OR $qs['TIPOEDITOR']=='CEN' OR $qs['TIPOEDITOR']=='CEA' ) {
		if (empty($tipa)) {
		// La tipologia annuncio e' vuota: prendo tutto i marcatori 
			$sql = "SELECT mrc_mrca_id    , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		    	FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI
			   WHERE mrc_tst_id      = :tstParam
			     AND mrc_edz_id      = :edzParam
			     AND mrc_rbra_seq_id = :rubrParam 
                 AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
			     AND mrca_id         = mrc_mrca_id
				 AND stlc_id    (+)  = mrc_stlc_id
			   ORDER BY mrc_pos_video";
			   
			$params = array(
				':tstParam' => $qs['TST'],
				':edzParam' => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ'],
			);
		}
		else {
		// La tipologia annuncio e' valorizzata: prendo solo i marcatori previsti da quella tipologia
			$sql = "SELECT mrc_mrca_id    , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		          FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI, TANNTIPMRC
		         WHERE atipm_tst_id      = :tstParam
		           AND atipm_edz_id      = :edzParam
		           AND atipm_rbra_seq_id = :rubrParam 
		           AND atipm_atipa_id    = :atipaParam
		           AND mrc_tst_id      = atipm_tst_id
		           AND mrc_edz_id      = atipm_edz_id
		           AND mrc_rbra_seq_id = atipm_rbra_seq_id
		           AND mrc_mrca_id     = atipm_mrc_id
                   AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
		           AND mrca_id = mrc_mrca_id
				   AND stlc_id    (+)  = mrc_stlc_id
		         ORDER BY mrc_pos_video";
				 
			$params = array(
				':tstParam'   => $qs['TST'],
				':edzParam'   => $qs['EDZ'],
				':rubrParam'  => $qs['RUBRSEQ'],
				':atipaParam' => $tipa,
			);
		}  
	}
	// Se il tipo editor e' = CES (editor semplice) cambio la select
	else {
		if (empty($tipa)) {
			$sql = "SELECT mrc_mrca_id    , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		    	FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI
			   WHERE mrc_tst_id      = :tstParam
			     AND mrc_edz_id      = :edzParam
			     AND mrc_rbra_seq_id = :rubrParam 
                 AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
			     AND mrca_id 		 = mrc_mrca_id
				 AND mrc_ord  IS NOT NULL
				 AND mrc_flag_spcf   = 0 /* NON gli specifici */
				 AND stlc_id    (+)  = mrc_stlc_id
			   ORDER BY mrc_ord";
			   
			$params = array(
				':tstParam' => $qs['TST'],
				':edzParam' => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ'],
			);
		}
		else {
			$sql = "SELECT mrc_mrca_id    , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		    	FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI, TANNTIPMRC
			   WHERE atipm_tst_id      = :tstParam
		         AND atipm_edz_id      = :edzParam
		         AND atipm_rbra_seq_id = :rubrParam 
				 AND atipm_atipa_id    = :atipaParam
		         AND mrc_tst_id      = atipm_tst_id
			     AND mrc_edz_id      = atipm_edz_id
			     AND mrc_rbra_seq_id = atipm_rbra_seq_id
		         AND mrc_mrca_id     = atipm_mrc_id
				 AND mrc_ord  IS NOT NULL
				 AND mrc_flag_spcf   = 0 /* NON gli specifici */
                 AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
			     AND mrca_id 		 = mrc_mrca_id
				 AND stlc_id    (+)  = mrc_stlc_id
			   ORDER BY mrc_ord";
			   
			$params = array(
				':tstParam'  => $qs['TST'],
				':edzParam'  => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ'],
				':atipaParam' => $tipa,
			);
		}
	}
	
//echo "sql  : ".$sql."<br>";
//echo "tst  : ".$qs['TST']."<br>";
//echo "rubrParam  : ".$qs['RUBRSEQ']."<br>";
//echo "atipaParam  : ".$tipa."<br>";
	$DB->db_query($sql, $params);
	
	$tags = array();
	$numrif = sizeof($rifs);
	while( $result = $DB->db_fetch_array()) {
		// Escludo i BOX dai marcatori normali: c'e' un checkbox apposito
		if ($result[2]!='B'){
			// Se il marcatore e' un riferimento, devo verificare che sia da inglobare nel testo
//echo "marcccc  : ".$result[0]."<br>";
//echo "marcccctp  : ".$result[2]."<br>";
			if ($result[2] == 'R'){
				// E' un marcatore di tipo riferimento
				$tags[$result[0]] = array(
					'descrizione' 	 => $result[1],
					'tipo'        	 => $result[2],
					'testo'		  	 => $result[3],
					'qtamax'	  	 => $result[4],
					'family'	  	 => $result[5], // FONT_FAMILY
					'size'		  	 => $result[6], // FONT_SIZE
					'style'	 	  	 => $result[7], // FONT_STYLE
					'weight'	  	 => $result[8], // FONT_WEIGHT
					'align'	 	  	 => $result[9], // ALIGN
					'color'	 	  	 => $result[10], // COLOR
					'iniziopar'		 => $result[11],
					'fineiniziopar'  => $result[12],
					'finepar'		 => $result[13],
					'flagfonte'	  	 => $result[14],
					'flagspcf'	  	 => $result[15],
					'leading'		 => $result[16],
					'spacebfr'		 => $result[17],
					'spaceaft'		 => $result[18],
					'rightind'		 => $result[19],
					'leftind'		 => $result[20],
					'firstline'		 => $result[21],
					'charsize'		 => $result[22],
					'height'		 => $result[23],
					'riferimento' 	 => 1
				 );
			}
			else {
				$tags[$result[0]] = array(
					'descrizione' 	 => $result[1],
					'tipo'        	 => $result[2],
					'testo'		  	 => $result[3],
					'qtamax'	  	 => $result[4],
					'family'	  	 => $result[5], // FONT_FAMILY
					'size'		  	 => $result[6], // FONT_SIZE
					'style'	 	  	 => $result[7], // FONT_STYLE
					'weight'	  	 => $result[8], // FONT_WEIGHT
					'align'	 	  	 => $result[9], // ALIGN
					'color'	 	  	 => $result[10], // COLOR
					'iniziopar'		 => $result[11],
					'fineiniziopar'  => $result[12],
					'finepar'		 => $result[13],
					'flagfonte'	  	 => $result[14],
					'flagspcf'	  	 => $result[15],
					'leading'		 => $result[16],
					'spacebfr'		 => $result[17],
					'spaceaft'		 => $result[18],
					'rightind'		 => $result[19],
					'leftind'		 => $result[20],
					'firstline'		 => $result[21],
					'charsize'		 => $result[22],
					'height'		 => $result[23],
					'riferimento' 	 => 0
				 );
			}
		}
		
	}
//print_r($tags);
     
	$tagaggs = array();
	// Editor testo aggiuntivo: solo in questo caso estraggo i marcatori specifici per il testo aggiuntivo
	if ( $qs['TIPOEDITOR']=='CEA' ) {
		if (empty($tipa)) {
		// La tipologia annuncio e' vuota: prendo tutto i marcatori 
			$sql = "SELECT mrc_mrca_id      , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		    	FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI
			   WHERE mrc_tst_id      = :tstParam
			     AND mrc_edz_id      = :edzParam
			     AND mrc_rbra_seq_id = :rubrParam 
				 AND mrc_flag_spcf   = 1
                 AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
			     AND mrca_id         = mrc_mrca_id
				 AND stlc_id    (+)  = mrc_stlc_id
			   ORDER BY mrc_pos_video";
			   
			$params = array(
				':tstParam' => $qs['TST'],
				':edzParam' => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ'],
			);
		}
		else {
		// La tipologia annuncio e' valorizzata: prendo solo i marcatori previsti da quella tipologia
			$sql = "SELECT mrc_mrca_id    , mrca_descriz   , mrc_mrca_tp, mrc_testo,
						   NVL( mrc_qta_max, 0 ) ,
						   stlc_font_family     , stlc_font_size , stlc_font_style,
						   stlc_font_weight     , stlc_align     , stlc_color     ,
						   stlc_inizio_paragrafo, stlc_chiusa_inizio_paragrafo, 
						   stlc_fine_paragrafo  , stlc_flag_fonte, mrc_flag_spcf,
						   stlc_leading, stlc_space_before,  stlc_space_after,
						   stlc_right_indent, stlc_left_indent, stlc_first_line,
						   stlc_char_size, stlc_height
		          FROM TSTILICARATTERI, TMARCATORIANAG, TMARCATORI, TANNTIPMRC
		         WHERE atipm_tst_id      = :tstParam
		           AND atipm_edz_id      = :edzParam
		           AND atipm_rbra_seq_id = :rubrParam 
		           AND atipm_atipa_id    = :atipaParam
		           AND mrc_tst_id      = atipm_tst_id
		           AND mrc_edz_id      = atipm_edz_id
		           AND mrc_rbra_seq_id = atipm_rbra_seq_id
				   AND mrc_flag_spcf   = 1
		           AND mrc_mrca_id     = atipm_mrc_id
                   AND (mrc_dt_annl IS NULL OR mrc_dt_annl >= TRUNC(SYSDATE))
		           AND mrca_id = mrc_mrca_id
				   AND stlc_id    (+)  = mrc_stlc_id
		         ORDER BY mrc_pos_video";
				 
			$params = array(
				':tstParam'   => $qs['TST'],
				':edzParam'   => $qs['EDZ'],
				':rubrParam'  => $qs['RUBRSEQ'],
				':atipaParam' => $tipa,
			);
		}
		
		$DB->db_query($sql, $params);
		
		while( $result = $DB->db_fetch_array()) {
			// Escludo i BOX dai marcatori normali: c'e' un checkbox apposito
			if ($result[2]!='B'){
				// Se il marcatore e' un riferimento, devo verificare che sia da inglobare nel testo
//echo "marcccc  : ".$result[0]."<br>";
//echo "marcccctp  : ".$result[2]."<br>";
				if ($result[2] == 'R'){
					// E' un marcatore di tipo riferimento
					$tagaggs[$result[0]] = array(
						'descrizione' 	 => $result[1],
						'tipo'        	 => $result[2],
						'testo'		  	 => $result[3],
						'qtamax'	  	 => $result[4],
						'family'	  	 => $result[5], // FONT_FAMILY
						'size'		  	 => $result[6], // FONT_SIZE
						'style'	 	  	 => $result[7], // FONT_STYLE
						'weight'	  	 => $result[8], // FONT_WEIGHT
						'align'	 	  	 => $result[9], // ALIGN
						'color'	 	  	 => $result[10], // COLOR
						'iniziopar'		 => $result[11],
						'fineiniziopar'  => $result[12],
						'finepar'		 => $result[13],
						'flagfonte'	  	 => $result[14],
						'flagspcf'	  	 => $result[15],
						'leading'		 => $result[16],
						'spacebfr'		 => $result[17],
						'spaceaft'		 => $result[18],
						'rightind'		 => $result[19],
						'leftind'		 => $result[20],
						'firstline'		 => $result[21],
						'charsize'		 => $result[22],
						'height'		 => $result[23],
						'riferimento' => 1
					 );
				}
				else {
					$tagaggs[$result[0]] = array(
						'descrizione' 	 => $result[1],
						'tipo'        	 => $result[2],
						'testo'		  	 => $result[3],
						'qtamax'	  	 => $result[4],
						'family'	  	 => $result[5], // FONT_FAMILY
						'size'		  	 => $result[6], // FONT_SIZE
						'style'	 	  	 => $result[7], // FONT_STYLE
						'weight'	  	 => $result[8], // FONT_WEIGHT
						'align'	 	  	 => $result[9], // ALIGN
						'color'	 	  	 => $result[10], // COLOR
						'iniziopar'		 => $result[11],
						'fineiniziopar'  => $result[12],
						'finepar'		 => $result[13],
						'flagfonte'	  	 => $result[14],
						'flagspcf'	  	 => $result[15],
						'leading'		 => $result[16],
						'spacebfr'		 => $result[17],
						'spaceaft'		 => $result[18],
						'rightind'		 => $result[19],
						'leftind'		 => $result[20],
						'firstline'		 => $result[21],
						'charsize'		 => $result[22],
						'height'		 => $result[23],
						'riferimento' => 0
					 );
				}
			}
			
		}
		
	}
	    
	// CASSETTI
	$sql = "SELECT auc_cassetto1, auc_cassetto2, 
                   auc_cassetto3, auc_cassetto4
             FROM TANNUTECASSETTI
            WHERE auc_ute_id = :uteParam";
	$params = array(':uteParam' => $qs['UTENTE']);
	$DB->db_query($sql, $params);

	$drawers = $DB->db_fetch_array(OCI_NUM);

	$tagTesto = "";
	
	if ($qs['TIPOEDITOR']=='CN') {
		$sql = "SELECT annd_dfnt_id, TRANSLATE (dfnt_denominazione, '������', 'oeeaui') dfnt_denominazione, dfnt_path_file||dfnt_nome_file dfnt_nome_file, dfnt_flag_foto
				FROM TDEFUNTI, TANNDEF
				WHERE annd_ann_eserc_id = :eserc_id
				AND   annd_ann_soc_id   = :soc_id
				AND   annd_ann_uop_id   = :uop_id
				AND   annd_ann_fil_id   = :fil_id
				AND   annd_ann_nmg_id   = :nmg_id
				AND   annd_ann_id       = :id
				AND   dfnt_id           = annd_dfnt_id ".
			  //" AND   dfnt_flag_foto    = 1 ".
			  " ORDER BY annd_ord";
		$params = array(
			':eserc_id' => $qs['ESERC'],
			':soc_id' 	=> $qs['SOC'],
			':uop_id' 	=> $qs['UOP'],
			':fil_id' 	=> $qs['FIL'],
			':nmg_id' 	=> $qs['NUM'],
			':id' 		=> $qs['ID'],
		);
		$DB->db_query($sql, $params);
		
		$defunti = array();
		$stringafoto="";
		
		while($result=$DB->db_fetch_array()) {
			foreach($tags as $tag_id => $tag_info) {
				if ($result[3] == 1){ //dfnt_flag_foto
				    // Il defunto ha la foto
    				if ($tag_info['tipo']=='F') {
    					$id_defunto = $result[0]."|".$tag_id;
							if (empty($tag_info['testo'])){
								$stringafoto = $tag_id." | ".$result[0]." | ".$result[1];
							} else{
								$stringafoto = $tag_id." | ".$result[0]." | ".$tag_info['testo']." ".$result[1];
							}
    					$defunti[$id_defunto] = array(
							'tag'     => $tag_id, 
    						'stringa' => $stringafoto, 
    						'nome'    => $tag_id." - ".$result[1],
    						'file'    => $result[2],
							'fteid'   => $result[0]
    					);
//    					$defunti[$id_defunto] = array(
//    						'stringa' => $tag_id." | ".$result[0]." | ".$tag_info['testo']." ".$result[1], 
//    						'nome' => $tag_id." - ".$result[1],
//    						'file' => $tag_id." - ".$result[2]
//    					);
/*
							$fotoannunci[$id_fotoannuncio] = array(
								'tag'     => $tag_id, 
								'stringa' => $stringafoto, 
								'nome'    => $tag_id." - ".$result[1],
								'file'    => $result[2],
								'fteid'   => $result[0]
							);
*/
    				}
				}
				if ($tag_info['tipo']=='N') {
				    // Nome defunto: nel testo metto il nome del defunto
					$tag_info['testo'] = $result[1];
					$tagTesto = $tag_info['testo'];
//echo "ttagTesto1 ".$tagTesto;
				}
			}
		}
	}
	if ($qs['TIPOEDITOR']=='CEN' or $qs['TIPOEDITOR']=='CEA' or $qs['TIPOEDITOR']=='CES' ) {
		$sql = "SELECT annf_fte_id, fte_descriz, fte_path_file||fte_nome_file fte_nome_file
				FROM TFOTOECON, TANNFOTOECON
				WHERE annf_ann_eserc_id = :eserc_id
				AND   annf_ann_soc_id   = :soc_id
				AND   annf_ann_uop_id   = :uop_id
				AND   annf_ann_fil_id   = :fil_id
				AND   annf_ann_nmg_id   = :nmg_id
				AND   annf_ann_id       = :id
				AND   fte_id            = annf_fte_id
				ORDER BY annf_ord";
		$params = array(
			':eserc_id' => $qs['ESERC'],
			':soc_id' 	=> $qs['SOC'],
			':uop_id' 	=> $qs['UOP'],
			':fil_id' 	=> $qs['FIL'],
			':nmg_id' 	=> $qs['NUM'],
			':id' 		=> $qs['ID'],
		);
		$DB->db_query($sql, $params);		
		
		$fotoannunci=array();
		$stringafoto="";

//foreach($tags as $tag_id => $tag_info) {
//  fwrite($fp_log,"      tag_id ".$tag_id." ".$tag_info['descrizione']."  - spcf:  ".$tag_info['flagspcf']." ".date("G:i:s").chr(13).chr(10));
//}
//fwrite($fp_log,"entro ".date("G:i:s").chr(13).chr(10));

		while($result=$DB->db_fetch_array()) {
//fwrite($fp_log,"ciclo ".date("G:i:s").chr(13).chr(10));
			foreach($tags as $tag_id => $tag_info) {
//fwrite($fp_log,"tag_info['tipo'] ".$tag_info['tipo']."  ".date("G:i:s").chr(13).chr(10));
				if ($tag_info['tipo']=='F') {
					if ( $qs['TIPOEDITOR']=='CEA' ){
						// Se il tipo editor = CEA (editor specifico), aggancio la foto solo se e' specifica
						if ( $tag_info['flagspcf'] == 1) {
							$id_fotoannuncio = $result[0]."|".$tag_id;
							// Se mrc_testo ( $tag_info['testo'] ) e' vuoto, non lo concateno proprio, per evitare di mettere uno spazio
							// in piu', che impedirebbe all'editor di riconoscere correttamente il marcatore come foto
							if (empty($tag_info['testo'])){
								$stringafoto = $tag_id." | ".$result[0]." | ".$result[1];
							} else{
								$stringafoto = $tag_id." | ".$result[0]." | ".$tag_info['testo']." ".$result[1];
							}
							$fotoannunci[$id_fotoannuncio] = array(
								'tag'     => $tag_id, 
								'stringa' => $stringafoto, 
								'nome'    => $tag_id." - ".$result[1],
								'file'    => $result[2],
								'fteid'   => $result[0]
							);
						}
					}
					else{
						if ( $qs['TIPOEDITOR']=='CES' ){
							// Se il tipo editor = CEA (editor specifico), aggancio la foto solo se e' specifica
							if ( $tag_info['flagspcf'] == 0) {
//  fwrite($fp_log,"         CES      tag_id ".$tag_id." ".$tag_info['descrizione']."  - spcf:  ".$tag_info['flagspcf']." ".date("G:i:s").chr(13).chr(10));
								$id_fotoannuncio = $result[0]."|".$tag_id;
								// Se mrc_testo ( $tag_info['testo'] ) e' vuoto, non lo concateno proprio, per evitare di mettere uno spazio
								// in piu', che impedirebbe all'editor di riconoscere correttamente il marcatore come foto
								if (empty($tag_info['testo'])){
									$stringafoto = $tag_id." | ".$result[0]." | ".$result[1];
								} else{
									$stringafoto = $tag_id." | ".$result[0]." | ".$tag_info['testo']." ".$result[1];
								}
								$fotoannunci[$id_fotoannuncio] = array(
									'tag'     => $tag_id, 
									'stringa' => $stringafoto, 
									'nome'    => $tag_id." - ".$result[1],
									'file'    => $result[2],
									'fteid'   => $result[0]
								);
							}
						}
						else{
							$id_fotoannuncio = $result[0]."|".$tag_id;
							// Se mrc_testo ( $tag_info['testo'] ) e' vuoto, non lo concateno proprio, per evitare di mettere uno spazio
							// in piu', che impedirebbe all'editor di riconoscere correttamente il marcatore come foto
							if (empty($tag_info['testo'])){
								$stringafoto = $tag_id." | ".$result[0]." | ".$result[1];
							} else{
								$stringafoto = $tag_id." | ".$result[0]." | ".$tag_info['testo']." ".$result[1];
							}
							$fotoannunci[$id_fotoannuncio] = array(
								'tag'     => $tag_id, 
								'stringa' => $stringafoto, 
								'nome'    => $tag_id." - ".$result[1],
								'file'    => $result[2],
								'fteid'   => $result[0]
							);
						}
					}
				}
			}
		}
	}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?><willbiteditor>
	<error number="<?php echo $errore;?>" description="<?php echo $errore_descrizione;?>" />
	<tags>
<?php
	$stile      = "";
	$fine_stile = "";
	// Nell'array seguente memorizzo i marcatori-riferimento che NON sono presenti nei riferimenti dell'annuncio
	$tagrifaggs = array();
	$countindex = 0;
	foreach($tags as $tag_id => $tag_info) {
		// 19.02.2011 - Se il tipo editor = CES (editor ad aree) non devo applicare il filtro sulle foto, in modo che
		// il marcatore compaia nella tag list dei marcatori e crei anche l'area destinata alle foto stesse;
		// negli altri casi la foto NON deve stare ella tag list dei marcatori ma solo in quella delle foto
		if ( $qs['TIPOEDITOR']=='CES' ) {
			if ($tag_info['flagfonte'] == '1') {
				//$stile      = " <font face=\"".$tag_info['fonte']."\" size=\"".$tag_info['corpo']."\">".$tag_info['stile'];
				$stile      = $tag_info['iniziopar'].$tag_info['family'].$tag_info['size'].$tag_info['style'].$tag_info['weight'].$tag_info['align'].$tag_info['color'].$tag_info['fineiniziopar'];
				$fine_stile = $tag_info['finepar'];
			}
			else {
				$stile      = "";
				$fine_stile = "";
			}
			
			
			if ( $tag_info['riferimento'] == 0 ){
				// Con il countindex definisco il tasto funzione associato: F1, F2... SOLO SE il tag NON e' riferimento
				$countindex += 1;
				/*echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" maxwords=\"".$tag_info['qtamax']
					."\" format_start=\"".base64_encode("".$stile."")
					."\" format_end=\"".base64_encode("".$fine_stile."") .  "\"  flag_rif=\"". $tag_info['riferimento']
					 ."\" />\n";*/
				/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commetare quelle precedenti*/
				echo "\t\t<tag label=\""."F$countindex - ".$tag_info['descrizione']."\" data=\"".$tag_id."\" type=\"".$tag_info['tipo']."\" maxwords=\"".$tag_info['qtamax']
					."\" format_start=\"".$stile
					."\" format_end=\"".$fine_stile
					."\"  flag_rif=\"". $tag_info['riferimento']
					."\"  leading=\"". $tag_info['leading']
					."\"  space_before=\"". $tag_info['spacebfr']
					."\"  space_after=\"". $tag_info['spaceaft']
					."\"  right_indent=\"". $tag_info['rightind']
					."\"  left_indent=\"". $tag_info['leftind']
					."\"  first_line=\"". $tag_info['firstline']
					."\"  chars_size=\"". $tag_info['charsize']
					."\"  height=\"". $tag_info['height']
					."\" />\n";
			}
			else{
				/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commetare quelle precedenti*/
				echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" type=\"".$tag_info['tipo']."\" maxwords=\"".$tag_info['qtamax']
					."\" format_start=\"".$stile
					."\" format_end=\"".$fine_stile
					."\"  flag_rif=\"". $tag_info['riferimento']
					."\"  leading=\"". $tag_info['leading']
					."\"  space_before=\"". $tag_info['spacebfr']
					."\"  space_after=\"". $tag_info['spaceaft']
					."\"  right_indent=\"". $tag_info['rightind']
					."\"  left_indent=\"". $tag_info['leftind']
					."\"  first_line=\"". $tag_info['firstline']
					."\"  chars_size=\"". $tag_info['charsize']
					."\"  height=\"". $tag_info['height']
					."\" />\n";
			}
			
		}
		else{
//echo "ttagTesto3 ".$tagTesto;
			if ($tag_info['tipo']!='F') {
				//			echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" maxwords=\"".$tag_info['qtamax']."\" format_start=\"".$tag_info['tpcar1']."\"/>\n";
				//			echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" maxwords=\"".$tag_info['qtamax']."\" format_start=\"".base64_encode("<b>")."\" format_end=\"".base64_encode("</b>")."\"/>\n";
				if ($tag_info['flagfonte'] == '1') {
					//$stile      = " <font face=\"".$tag_info['fonte']."\" size=\"".$tag_info['corpo']."\">".$tag_info['stile'];
					$stile      = $tag_info['iniziopar'].$tag_info['family'].$tag_info['size'].$tag_info['style'].$tag_info['weight'].$tag_info['align'].$tag_info['color'].$tag_info['fineiniziopar'];
					$fine_stile = $tag_info['finepar'];
				}
				else {
					//$stile      = $tag_info['stile'];
					//$fine_stile = $tag_info['finestile'];
					$stile      = "";
					$fine_stile = "";
				}
				
				if ( $qs['TIPOEDITOR']=='CN' OR $qs['TIPOEDITOR']=='CEN' ){
					// Con il countindex definisco il tasto funzione associato: F1, F2...
					$countindex += 1;
					/*echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" maxwords=\"".$tag_info['qtamax']
						."\" format_start=\"".base64_encode("".$stile."")
						."\" format_end=\"".base64_encode("".$fine_stile."") .  "\"  flag_rif=\"". $tag_info['riferimento']
						 ."\" />\n";*/
					/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commetare quelle precedenti*/
					if ($tag_info['tipo'] == 'N') {
//echo "ttagTesto4 ".$tagTesto;
    					echo "\t\t<tag label=\""."F$countindex - ".$tag_info['descrizione']."\" data=\"".$tag_id."\" text=\"".$tagTesto."\" maxwords=\"".$tag_info['qtamax']
    						."\" format_start=\"".$stile
    						."\" format_end=\"".$fine_stile
							."\"  flag_rif=\"". $tag_info['riferimento']
							."\"  leading=\"". $tag_info['leading']
							."\"  space_before=\"". $tag_info['spacebfr']
							."\"  space_after=\"". $tag_info['spaceaft']
							."\"  right_indent=\"". $tag_info['rightind']
							."\"  left_indent=\"". $tag_info['leftind']
							."\"  first_line=\"". $tag_info['firstline']
							."\"  chars_size=\"". $tag_info['charsize']
							."\"  height=\"". $tag_info['height']
    						."\" />\n";
					} 
					else{
					echo "\t\t<tag label=\""."F$countindex - ".$tag_info['descrizione']."\" data=\"".$tag_id."\" text=\"".$tag_info['testo']."\" maxwords=\"".$tag_info['qtamax']
						."\" format_start=\"".$stile
						."\" format_end=\"".$fine_stile
						."\"  flag_rif=\"". $tag_info['riferimento']
						."\"  leading=\"". $tag_info['leading']
						."\"  space_before=\"". $tag_info['spacebfr']
						."\"  space_after=\"". $tag_info['spaceaft']
						."\"  right_indent=\"". $tag_info['rightind']
						."\"  left_indent=\"". $tag_info['leftind']
						."\"  first_line=\"". $tag_info['firstline']
						."\"  chars_size=\"". $tag_info['charsize']
						."\"  height=\"". $tag_info['height']
						."\" />\n";
				    }
				}
				else{
					if ( $tag_info['riferimento'] == 0 ){
						// Con il countindex definisco il tasto funzione associato: F1, F2... SOLO SE il tag NON e' riferimento
						$countindex += 1;
						/*echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" maxwords=\"".$tag_info['qtamax']
							."\" format_start=\"".base64_encode("".$stile."")
							."\" format_end=\"".base64_encode("".$fine_stile."") .  "\"  flag_rif=\"". $tag_info['riferimento']
							 ."\" />\n";*/
						/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commentare quelle precedenti*/
						echo "\t\t<tag label=\""."F$countindex - ".$tag_info['descrizione']."\" data=\"".$tag_id."\" text=\"".$tag_info['testo']."\" maxwords=\"".$tag_info['qtamax']
							."\" format_start=\"".$stile
							."\" format_end=\"".$fine_stile
							."\"  flag_rif=\"". $tag_info['riferimento']
							."\"  leading=\"". $tag_info['leading']
							."\"  space_before=\"". $tag_info['spacebfr']
							."\"  space_after=\"". $tag_info['spaceaft']
							."\"  right_indent=\"". $tag_info['rightind']
							."\"  left_indent=\"". $tag_info['leftind']
							."\"  first_line=\"". $tag_info['firstline']
							."\"  chars_size=\"". $tag_info['charsize']
							."\"  height=\"". $tag_info['height']
							."\" />\n";
					}
					else{
						echo "\t\t<tag label=\"".$tag_info['descrizione']."\" data=\"".$tag_id."\" text=\"".$tag_info['testo']."\" maxwords=\"".$tag_info['qtamax']
							."\" format_start=\"".$stile
							."\" format_end=\"".$fine_stile
							."\"  flag_rif=\"". $tag_info['riferimento']
							."\"  leading=\"". $tag_info['leading']
							."\"  space_before=\"". $tag_info['spacebfr']
							."\"  space_after=\"". $tag_info['spaceaft']
							."\"  right_indent=\"". $tag_info['rightind']
							."\"  left_indent=\"". $tag_info['leftind']
							."\"  first_line=\"". $tag_info['firstline']
							."\"  chars_size=\"". $tag_info['charsize']
							."\"  height=\"". $tag_info['height']
							."\" />\n";
					}
				}
			}
		}
		
		// Creo un array con i marcatori di tipo riferimento NON presenti tra i riferimenti dell'annuncio
		if ( $tag_info['riferimento'] == 1 ) {
			$trovato = 0;
			foreach($rifs as $mrcf_id => $mrcf_info) {
				// Verifico se nell'elenco complessivo dei marcatori ci sono dei marcatori gia' presenti nei riferimenti (MRC_RIF) o nel testo (MRC)
				if ($mrcf_info['MRC_RIF'] == $tag_id OR $mrcf_info['MRC'] == $tag_id){
					$trovato = 1;
				}
			}
			if( $trovato == 0 ){
					$tagrifaggs[$tag_id] = array(
						'descrizione' 	 => $tag_info['descrizione'],
						'tipo'        	 => $tag_info['tipo'],
						'testo'		  	 => $tag_info['testo'],
						'qtamax'	  	 => $tag_info['qtamax'],
						'family'	  	 => $tag_info['family'], // FONT_FAMILY
						'size'		  	 => $tag_info['size'], // FONT_SIZE
						'style'	 	  	 => $tag_info['style'], // FONT_STYLE
						'weight'	  	 => $tag_info['weight'], // FONT_WEIGHT
						'align'	 	  	 => $tag_info['align'], // ALIGN
						'color'	 	  	 => $tag_info['color'], // COLOR
						'iniziopar'		 => $tag_info['iniziopar'],
						'fineiniziopar'  => $tag_info['fineiniziopar'],
						'finepar'		 => $tag_info['finepar'],
						'flagfonte'	  	 => $tag_info['flagfonte'],
						'riferimento' => 1
					 );
			}
				
		}
	}
//	print_r($rifs);
//	print_r($tagrifaggs);
?>
	</tags>
	<tagrifs>
<?php
	// Marcatori di tipo riferimento non presenti nel testo ne' tra i riferimenti dell'annuncio (TANNUNCIRIF)
	$stile      = "";
	$fine_stile = "";
	foreach($tagrifaggs as $tagr_id => $tagr_info) {
		if ($tagr_info['tipo']!='F') {
			if ($tagr_info['flagfonte'] == '1') {
				$stile      = $tagr_info['iniziopar'].$tagr_info['family'].$tagr_info['size'].$tagr_info['style'].$tagr_info['weight'].$tagr_info['align'].$tagr_info['color'].$tagr_info['fineiniziopar'];
				$fine_stile = $tagr_info['finepar'];
				/*if ( !empty($tagr_info['tpall']) ) {
					$stile      .= " <p align=\"".$tagr_info['tpall']."\">";
					$fine_stile = "</p>".$tagr_info['finestile'].$tagr_info['finefonte'];
				}
				else {
					$fine_stile = $tagr_info['finestile'].$tagr_info['finefonte'];
				}*/
			}
			else {
				//$stile      = $tagr_info['stile'];
				//$fine_stile = $tagr_info['finestile'];
				$stile      = "";
				$fine_stile = "";
			}
			
			/*echo "\t\t<tag label=\"".$tagr_info['descrizione']."\" data=\"".$tagr_id."\" maxwords=\"".$tagr_info['qtamax']
				."\" format_start=\"".base64_encode("".$stile."")
				."\" format_end=\"".base64_encode("".$fine_stile."") .  "\"  flag_rif=\"". $tagr_info['riferimento']
				 ."\" />\n";*/
			/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commetare quelle precedenti */
			echo "\t\t<tag label=\"".$tagr_info['descrizione']."\" data=\"".$tagr_id."\" maxwords=\"".$tagr_info['qtamax']
				."\" format_start=\"".$stile
				."\" format_end=\"".$fine_stile.  "\"  flag_rif=\"". $tagr_info['riferimento']
				 ."\" />\n";
		}
		
	}
?>
	</tagrifs>
	<tagaggs>
<?php
	// Marcatori specifici testo aggiuntivo
	$stile      = "";
	$fine_stile = "";
	$countindex = 0;
	foreach($tagaggs as $taga_id => $taga_info) {
		if ($taga_info['tipo']!='F') {
			if ($taga_info['flagfonte'] == '1') {
				//$stile      = " <font face=\"".$taga_info['fonte']."\" size=\"".$taga_info['corpo']."\">".$taga_info['stile'];
				$stile      = $taga_info['iniziopar'].$taga_info['family'].$taga_info['size'].$taga_info['style'].$taga_info['weight'].$taga_info['align'].$taga_info['color'].$taga_info['fineiniziopar'];
				$fine_stile = $taga_info['finepar'];
				/*if ( !empty($taga_info['tpall']) ) {
					$stile      .= " <p align=\"".$taga_info['tpall']."\">";
					$fine_stile = "</p>".$taga_info['finestile'].$taga_info['finefonte'];
				}
				else {
					$fine_stile = $taga_info['finestile'].$taga_info['finefonte'];
				}*/
			}
			/*else {
				$stile      = $taga_info['stile'];
				$fine_stile = $taga_info['finestile'];
			}*/
			
			// Con il countindex definisco il tasto funzione associato: F1, F2...
			$countindex += 1;
			/*echo "\t\t<tag label=\"".$taga_info['descrizione']."\" data=\"".$taga_id."\" maxwords=\"".$taga_info['qtamax']
				."\" format_start=\"".base64_encode("".$stile."")
				."\" format_end=\"".base64_encode("".$fine_stile."") .  "\"  flag_rif=\"". $taga_info['riferimento']
				 ."\" />\n";*/
			/* Per capire cosa c'e' scritto dentro scommentare le righe seguenti e commetare quelle precedenti */
			echo "\t\t<tag label=\""."F$countindex - ".$taga_info['descrizione']."\" data=\"".$taga_id."\" maxwords=\"".$taga_info['qtamax']
				."\" format_start=\"".$stile
				."\" format_end=\"".$fine_stile
				."\"  flag_rif=\"". $taga_info['riferimento']
				."\"  leading=\"". $taga_info['leading']
				."\"  space_before=\"". $taga_info['spacebfr']
				."\"  space_after=\"". $taga_info['spaceaft']
				."\"  right_indent=\"". $taga_info['rightind']
				."\"  left_indent=\"". $taga_info['leftind']
				."\"  first_line=\"". $taga_info['firstline']
				."\"  chars_size=\"". $taga_info['charsize']
				."\"  height=\"". $taga_info['height']
				."\" />\n";
		}
		
	}
?>
	</tagaggs>
	<symbols>
		<symbol label="amp" data="SYM_AMPERSAND"/>
		<symbol label="ss" data="SYM_SCISSOR"/>
		<symbol label="ww" data="SYM_SHARP"/>
		<symbol label="q" data="SYM_AT"/>
		<symbol label="l" data="SYM_POUND"/>
		<symbol label="^" data="SYM_POWER"/>
	</symbols>
	<frame isframed="<?php echo ($riqIniz=="") ? "false" : "true" ?>"/>
	<drawers>
<?php
	
	foreach($drawers as $drawer) {
		echo "\t\t<drawer><![CDATA[".htmlspecialchars_decode($drawer)."]]></drawer>\n";
	}
	
	//Converto il testo da 8859-15 a UTF8
	$data = $NewEncoding->Convert($data);
	
?>
		<drawer><![CDATA[<?php echo $qs['IMPRESA'];?>]]></drawer>
	</drawers>
	<textruns>
<?php
	if ( isset( $flagfotoobbl ) ){
		// Se la variabile $flagfotoobbl e' settata, vuol dire che e' attivo il controllo sul flag foto obbligatoria
	//	echo "\t\t<parameters><![CDATA[<parametri fotoobbl =\"".$flagfotoobbl."\" />]]></parameters>\n";
		echo "\t\t<parameters><parametri fotoobbl =\"".$flagfotoobbl."\" /></parameters>\n";
	} else {
		echo "\t\t<parameters><![CDATA[]]></parameters>\n";
	}
?>
		<maintext><![CDATA[<?php echo htmlspecialchars_decode( $data ); ?>]]></maintext>
	</textruns>
<?php 
	if ($qs['TIPOEDITOR']=='CN') {
		echo "\t<immagini tipo=\"Defunti\">\n";
		foreach($defunti as $defunto_desc) {
			// echo "\t\t<immagine label=\"".$defunto_desc['nome']."\" data=\"".$defunto_desc['stringa']."\"/>\n";
			foreach($tags as $tag_id => $tag_info) {
				if ($tag_id == $defunto_desc['tag'] ) {
					$trovato = 1;
					$zero = "0";
					
					if ($tag_info['flagfonte'] == '1') {
						$stile      = $tag_info['iniziopar'].$tag_info['family'].$tag_info['size'].$tag_info['style'].$tag_info['weight'].$tag_info['align'].$tag_info['color'].$tag_info['fineiniziopar'];
						$fine_stile = $tag_info['finepar'];
						$spacebfr = $tag_info['spacebfr'];
						$spaceaft = $tag_info['spaceaft'];
						$height = $tag_info['height'];
					}
					else {
						$stile      = "";
						$fine_stile = "";
						$spacebfr 	= "";
						$spaceaft 	= "";
						$height 	= "";
					}
				}
			}
			echo "\t\t<immagine label=\"".$defunto_desc['nome']
			        ."\" data=\"".$defunto_desc['stringa']
					."\" tag=\"".$defunto_desc['tag']
					."\" fteid=\"".$defunto_desc['fteid']
					."\" file=\"".$defunto_desc['file']
					."\" format_start=\"".$stile
					."\" format_end=\"".$fine_stile
					."\"  flag_rif=\"". $tag_info['riferimento']
					."\"  leading=\"". $zero
					."\"  space_before=\"". $spacebfr
					."\"  space_after=\"". $spaceaft
					."\"  right_indent=\"". $zero
					."\"  left_indent=\"". $zero
					."\"  first_line=\"". $zero
					."\"  chars_size=\"". $zero
					."\"  height=\"". $height
					. "\" />\n";
		}
		echo "\t</immagini>\n";
	} else /*if ($qs['TIPOEDITOR']=='CEN' or $qs['TIPOEDITOR']=='CEA' )*/ {
		echo "\t<immagini tipo=\"Economici\">\n";
		$trovato = 0;
		foreach($fotoannunci as $foto_desc) {
//		foreach($fotoannunci as $id_annuncio => $foto_desc) {
			foreach($tags as $tag_id => $tag_info) {
				if ($tag_id == $foto_desc['tag'] ) {
					$trovato = 1;
					$zero = "0";
					
					if ($tag_info['flagfonte'] == '1') {
						$stile      = $tag_info['iniziopar'].$tag_info['family'].$tag_info['size'].$tag_info['style'].$tag_info['weight'].$tag_info['align'].$tag_info['color'].$tag_info['fineiniziopar'];
						$fine_stile = $tag_info['finepar'];
						$spacebfr = $tag_info['spacebfr'];
						$spaceaft = $tag_info['spaceaft'];
						$height = $tag_info['height'];
					}
					else {
						$stile      = "";
						$fine_stile = "";
						$spacebfr 	= "0";
						$spaceaft 	= "0";
						$height 	= "0";
					}
				}
			}
			echo "\t\t<immagine label=\"".$foto_desc['nome']
				."\" data=\"".$foto_desc['stringa']
				."\" tag=\"".$foto_desc['tag']
				."\" fteid=\"".$foto_desc['fteid']
				."\" file=\"".$foto_desc['file']
				."\" format_start=\"".$stile
				."\" format_end=\"".$fine_stile
				."\"  flag_rif=\"". $tag_info['riferimento']
				."\"  leading=\"". $zero
				."\"  space_before=\"". $spacebfr
				."\"  space_after=\"". $spaceaft
				."\"  right_indent=\"". $zero
				."\"  left_indent=\"". $zero
				."\"  first_line=\"". $zero
				."\"  chars_size=\"". $zero
				."\"  height=\"". $height
				. "\" />\n";
		}
		echo "\t</immagini>\n";
	} /*else
	{
		echo "\t<immagini>\n";
		echo "\t</immagini>\n";
	}*/

$fp_log=fopen("LOG_LOADTEXT".".txt",'a+');
fwrite($fp_log, date("Y-m-d G:i:s")." - FINE".chr(13).chr(10) );
fclose($fp_log);
	
?>
</willbiteditor>
