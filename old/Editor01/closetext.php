<?php
// Modifiche
// 24.04.2015 - Gigi - if (count($query) =! 13) diventa if (count($query) < 13) - Gestione di 15 parametri (foto obbl, tipo tabellare e qta' moduli)
// 14.07.2015 - Steve - gestione del parametri per le necro tabellari
// 2016.02.03 - Gigi - Commentata scrittura LOG

	require('./include/db_connect.php');

	// per prima cosa faccio un test se ID mi viene correttamente passato
	// in caso contrario genero una response di errore
	$errore = 0;
	$errore_descrizione = "";

	$id = "";
	if (isset($_REQUEST['id'])) {
		$id = $_REQUEST['id'];
	}

// var_dump($id);
	
	if ($id == "") 
	{
		$errore = 10000;
		$errore_descrizione = htmlspecialchars("TEXT ID IS UNDEFINED\nThe parent html page does not have and pass parameter: ID");
	}
	
	$query = explode("|", $id);

	if (count($query) < 13)	
	{
		$errore = 10001;
		$errore_descrizione = htmlspecialchars("WRONG PARAMETERS NUMBER\nYou pass id=".$id);
	} 
	else 
	{
		if (array_key_exists('FLAGTABELLARE', $query)) {
		 
			$qs = array (
				'ESERC'    		=> $query[0],
				'SOC' 	   		=> $query[1],
				'UOP' 	   		=> $query[2],
				'FIL' 	   		=> $query[3],
				'NUM' 	   		=> $query[4],
				'ID' 	   		=> $query[5],
				'UTENTE'   		=> $query[6],
				'TST' 	   		=> $query[7],
				'EDZ' 	   		=> $query[8],
				'RUBRSEQ'  		=> $query[9],
				'ATIPA'    		=> $query[10],
				'IMPRESA'  		=> $query[11],
				'TIPOEDITOR' 	=> $query[12],
				'FLAGFOTOOBBL'  => $query[13],
				'FLAGTABELLARE' => $query[14],
				'MODLARGHEZZA'  => $query[15],
				'MODALTEZZA'    => $query[16],
				'MODULI'    	=> $query[17]
			);
		} else {
	         $qs = array (
				'ESERC'    		=> $query[0],
				'SOC' 	   		=> $query[1],
				'UOP' 	   		=> $query[2],
				'FIL' 	   		=> $query[3],
				'NUM' 	   		=> $query[4],
				'ID' 	   		=> $query[5],
				'UTENTE'   		=> $query[6],
				'TST' 	   		=> $query[7],
				'EDZ' 	   		=> $query[8],
				'RUBRSEQ'  		=> $query[9],
				'ATIPA'    		=> $query[10],
				'IMPRESA'  		=> $query[11],
				'TIPOEDITOR' 	=> $query[12],
				'FLAGFOTOOBBL'  => $query[13] ) ; 			
		}
		

//$fp_log=fopen("LOG_CLOSETEXT".".txt",'a+');
//fwrite($fp_log, date("Y-m-d G:i:s")." - count 18 ".$id.chr(13).chr(10) );
//foreach($qs as $k => $v) {
//  fwrite($fp_log, date("Y-m-d G:i:s")." - key ".$k." value ".$v.chr(13).chr(10));
//}
//fclose($fp_log);
		
 // qui mettere una chiamata per la close text    

//		$ute_id = ""; $ute_descriz = ""; $flag_apri="";
		//    $sql = "BEGIN  PCK_ANNUNCI.PP_CHIUDI_EDITOR(".
		//    		"'".$qs['ESERC']."',".
		//    		"'".$qs['SOC']."',".
		//    		"'".$qs['UOP']."',".
		//    		"'".$qs['FIL']."',".
		//    		"'".$qs['NUM']."',".
		//    		"'".$qs['ID']."'); END;";
				
		$sql = "BEGIN  PCK_ANNUNCI.PP_CHIUDI_EDITOR".
		           "(:ese, :soc, :uop, :fil, :nmg, :id);".
				   " END;";
/*		$params = array(
	        ":o_flag_apri"		=> array(&$flag_apri, 10, null),
	        ":o_ute_id"			=> array(&$ute_id, 50, null),
	        ":o_ute_descriz"	=> array(&$ute_descriz, 50, null)
		); */
		
			$params = array(
				':ese' 			=> $qs['ESERC'],
				':soc' 			=> $qs['SOC'],
				':uop' 			=> $qs['UOP'],
				':fil' 			=> $qs['FIL'],
				':nmg' 			=> $qs['NUM'],
				':id' 			=> $qs['ID']   );		
				
		$DB->db_query($sql, $params);
		

	} 

	// questo pezzo dopo lasciarlo, mi aspetto sempre un messaggio di errore se c'� o 0 se tutto ok
	
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
?><willbiteditor>
	<error number="<?php echo $errore;?>" description="<?php echo $errore_descrizione;?>" />
</willbiteditor>
