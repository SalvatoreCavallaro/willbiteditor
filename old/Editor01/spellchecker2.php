<?php

header('Content-Type: text/plain; charset=utf-8'); 

import_request_variables("gP");

$K_OPEN_TAG = "{";
$K_CLOSE_TAG = "}";

if ($lang=="") $lang="it";

// costruzione nome dizionario custom
$nome_custom_dict = "wbeditor_".$lang.".pws";
$fullpath_custom_dict =  dirname(__FILE__)."\\".$nome_custom_dict;

$int2 = pspell_new_personal($fullpath_custom_dict, $lang);

if (strlen($addkey) > 0)
{
	// aggiunta chiave in dizionario
	pspell_add_to_personal($int2, urldecode($addkey));
	pspell_save_wordlist($int2);
	echo "added:".urldecode($addkey);
	exit();
}

$txt = urldecode($txt);

// pulizia caratteri inutili

$to_clean = array("&lt;", "&gt;");
$cleaned_0_txt = str_replace($to_clean, "    ", $txt);

$to_clean = array("\"", "'", ",", ";", ":", ".", "|", "/", "+", "-", "=", "(", ")","\t", "\n", "\r");
$cleaned_1_txt = str_replace($to_clean, " ", $cleaned_0_txt);

// qui sostituisco i tag con spazi
// senza preg_replace

$copychar = true;
$cleaned_2_txt = "";
for ($i=0; $i<strlen($cleaned_1_txt); $i++)
{
	if (substr($cleaned_1_txt, $i, 1) == $K_OPEN_TAG)
		$copychar = false;
	
	if ($copychar)
		$cleaned_2_txt .= substr($cleaned_1_txt, $i, 1);
	else
		$cleaned_2_txt .= " ";
	
	if (substr($cleaned_1_txt, $i, 1) == $K_CLOSE_TAG)
		$copychar = true;
}

// qui ottengo un array con le parole da correggere e chiave posizione

$pos_words_array = str_word_count($cleaned_2_txt, 2);

// istanzio correttore

pspell_config_create($lang, PSPELL_NORMAL);
$int = pspell_new($lang,"","","utf-8");

// e qui costruisco il file di correzione

$j = 0;
while ($word = current($pos_words_array)) 
{
		if ((!pspell_check($int, $word)) && (!pspell_check($int2, $word)) ) 
		{
   				if ($j > 0)
   					echo "|";
   				
			    $suggestions = pspell_suggest($int, $word);
			    echo $word."/".key($pos_words_array)."/".(key($pos_words_array)+strlen($word)).":";
			    
			    $i = 0;
			    foreach ($suggestions as $suggestion) 
			    {
			    	if ($i > 0)
			    		echo ";";
						echo $suggestion;
						$i++;
   				}
   				
   				$j++;
		}
    
    next($pos_words_array);
}
?>