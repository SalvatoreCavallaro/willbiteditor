<?php

require ("config.inc.php");

class db_connect {
	var $host, $port, $sid, $service;
	var $connStr;
	var $connection;
	var $query;
	var $total_record, $rec_position;
	var $total_fields, $field_name;

	/**
		Costruttore: si connette al database
	*/
	function db_connect() {
		global $editor_config;
		
		$this->host = "" . $editor_config['ORCL_HOST'] . "";
		$this->port = "" . $editor_config['ORCL_PORT'] . "";
		$this->sid = "" . $editor_config['ORCL_SID'] . "";
		$this->service = "" . $editor_config['ORCL_SERVICE'] . "";

        if($this->sid=="") {
		$this->connStr = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (COMMUNITY = tcp.world) (PROTOCOL = TCP)(Host = ".$this->host.")(Port=".$this->port."))) (CONNECT_DATA = (SERVICE_NAME = ".$this->sid.")))";
	    	
		}else {
		$this->connStr = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (COMMUNITY = tcp.world) (PROTOCOL = TCP)(Host = ".$this->host.")(Port=".$this->port."))) (CONNECT_DATA = (SID = ".$this->sid.")))";
	    }

		$this->connection = oci_connect($editor_config['ORCL_USER_NAME'],
										$editor_config['ORCL_PASSWORD'],
										$this->connStr)
		                    or die ($this->get_error_msg($this->connection,"Problem while connecting to " . $this->host . " server..."  )) ;
 
/*		$this->connection = OCILogon($editor_config['ORCL_USER_NAME'],
									 $editor_config['ORCL_PASSWORD'],
									 $this->connStr)
		or die ($this->get_error_msg($this->connection,"Problem while connecting to " . $this->host . " server..."  ));
*/
	}

	/**
		This function query at database
	*/
	function db_query($query_str="", $params=null)
	{
    	if($query_str=="") {
	 		return false;
		}
	    $this->sql=$query_str;
	    $this->rec_position=0;
	    $this->query = OCI_parse($this->connection, $this->sql);

		if (is_array($params)) {
			foreach(array_keys($params) as $key) {
				if (is_array($params[$key])) {
					$constr = $params[$key];

					if ($constr[2]!=null) {
						// Se la variabile da bindare � un ARRAY usa la funzione apposita per gli array
						if (is_array($constr[0])) {
							oci_bind_array_by_name($this->query, $key, $constr[0], sizeof($constr[0]), $constr[1], $constr[2]);
						} // Altrimenti usa la funzione di bind classica delle variabili semplici
						else {
							oci_bind_by_name($this->query, $key, $constr[0], $constr[1], $constr[2]);
						}
					} else {
						if (is_array($constr[0])) {
							oci_bind_array_by_name($this->query, $key, $constr[0], sizeof($constr[0]), $constr[1]);
						} // Altrimenti usa la funzione di bind classica delle variabili semplici
						else {
							oci_bind_by_name($this->query, $key, $constr[0], $constr[1]);
						}
					}
				} else {
					oci_bind_by_name($this->query, $key, $params[$key]);
				}
			}
		}
		
	    OCI_execute($this->query, OCI_DEFAULT)
	    or die($this->get_error_msg($this->query ,"Query Error : ".$this->sql));
	}

	function db_commit()
	{
		if ($this->connection) {
			$committed = oci_commit($this->connection);
			// Test whether commit was successful. If error occurred, return error message
			if (!$committed) {
			    $error = oci_error($conn);
			    echo 'Commit failed. Oracle reports: ' . $error['message'];
			}
		}
	}	

	function db_get_sequence($seq_name) {
		if ($this->connection) {
			$sql = "SELECT ".$seq_name.".NEXTVAL FROM DUAL";
			$this->db_query($sql);
			if ($result = $this->db_fetch_array()) {
				return $result[0];
			}
		}
		return null;
	}

	function db_get_collection($type, $schema='') {
		if ($schema == '')
			$coll = oci_new_collection($this->connection, $type);
		else
			$coll = oci_new_collection($this->connection, $type, $schema);
			
		return $coll;
	}

	function db_free_collection($coll) {
		return $coll->free();
	}
	
	function db_append_collection($coll, $value) {
		return $coll->append($value);
	}
	
	function db_get_collection_size($coll) {
		return $coll->size();
	}

	/**
		This function creates a selectBox based on the stored sql query
		The 1st element of the array is used as "value"
		The 2nd element of the array is used as "text"
	*/
    function db_fetch_selectBox($selected='', $name='select', $multiple='', $size='', $parameter=NULL, $empty_value=false)
    {
        $output="<select name='$name' $multiple $size $parameter>";
        if ($empty_value)
			$output.="<option value=''>-- Selezionare --</option>";
        while($result=$this->db_fetch_array())
        {
            if($result[0]==trim($selected)) {
                $a="selected";   
            } else {
                $a="";
            }
            $output.="<option value='".$result[0]."' $a>".$result[1]."</option>";
        }
        $output.="</select>";
        return $output;
    }

	function db_fetch_xml_header() {
		return '<?xml version="1.0" encoding="ISO-8859-1"?><root>';
	}

	function db_fetch_xml_footer() {
		return '</root>';
	}

	/**
		Restituisce un XML formattato dai dati estratti dal database
		$rowName = nome da dare all'oggetto relativo alla riga di database
		$fieldNames = se presente, assegna i tag degli elementi ai nomi nel vettore
					  se non presente, usa come tag degli elementi i nomi dei campi estratti dal DB
	*/
	function db_fetch_xml ($rowName, $fieldNames='', $header=true, $footer=true) {
		$xml = '';
		if ($header) 
			$xml .= '<?xml version="1.0" encoding="ISO-8859-1"?><root>';

		if($fieldNames=='') {
			while ($result=$this->db_fetch_array(OCI_ASSOC)) {
				$xml .= "<" .$rowName. ">";
				foreach(array_keys($result) as $key) {
					$xml .= "<" .$key. ">" .$result[$key]. "</" .$key. ">";
				}
				$xml .= "</" .$rowName. ">";
			}
		} else {
			while ($result=$this->db_fetch_array(OCI_NUM)) {
				$xml .= "<"  .$rowName. ">";
				for ($i=0; $i<sizeof($fieldNames); $i++) {
					$xml .= "<" .$fieldNames[$i]. ">" .$result[$i]. "</" .$fieldNames[$i]. ">";
				}
				$xml .= "</" .$rowName. ">";
			}
		}
		if ($footer)
			$xml .= '</root>';
		
		return $xml;
	}

	// Scrollbar param: n = no scrollbar, y
	function db_fetch_table (
		$name='table',
		$css="",
		$colname='y',
		$hiddencols='',
		$return_field='',
		$t_params='',
		$r_params='',
		$c_params='',
		$scrollbar='0'
	) {
		$css_table = ($css!="") 	? "class=".$css."_dataOutline" 	: "";
		$css_header = ($css!="") 	? "class=".$css."_listHeader" 	: "";
		$css_row1 = ($css!="") 		? "class=".$css."_listRow1" 	: "";
		$css_row2 = ($css!="") 		? "class=".$css."_listRow2" 	: "";
		$css_rowHover = ($css!="") 	? $css."_listRowHover" : "";
		
		$hidden_cols_array = explode(',', $hiddencols);
		$output = '';

		if($scrollbar>0) {
			$output .= "<div id='DIV_$name' style='height:".$scrollbar."px; overflow:auto;'>";
		}	
		$output .= "<table name='$name' $css_table cellpadding='2' cellspacing='0' border='0' $t_params>";

		$return_column=-1;
		if(!empty($colname)) {
			$output .= "<thead><tr $css_header>";
			
			for ($i=0; $i<$this->get_num_fields(); $i++) {
				if (array_search($this->get_field_name($i), $hidden_cols_array)!==FALSE)
					$output .= "<td style='display:none; width:0px'>";
				else
					$output .= "<td>";
				$output .= $this->get_field_name($i)."</td>";

				if ($this->get_field_name($i) == $return_field)
					$return_column=$i;
			}
			$output .= "</tr></thead>";
		}

/*		if ($scrollbar>0) {
			$output .= "</table><div id='DIV_$name' style='height:".$scrollbar."px; overflow:auto;'>";
			$output .= "<table name='".$name."_ROWS' $css_table cellpadding='2' cellspacing='0' border='0' $t_params>";
		}
*/		
		$output .= "<tbody>";
		while($result=$this->db_fetch_array()) {
			if ($k%2==0)
				$output .= "<tr $r_params $css_row1 onmouseover=\"this.className='$css_rowHover';\" onmouseout=\"this.className='".$css."_listRow1';\">";
			else
				$output .= "<tr $r_params $css_row2 onmouseover=\"this.className='$css_rowHover';\" onmouseout=\"this.className='".$css."_listRow2';\">";
			for ($j=0; $j<$this->get_num_fields(); $j++) {
			    $cname=$this->get_field_name($j);
				$output .= "<td $c_params id='".($k).",".($j);
				if ($return_column>=0) {
					$output .= ",$return_column";
				}
				$output .= "' ";
				if (array_search($this->get_field_name($j), $hidden_cols_array)!==FALSE) {
					$output .= "style='display:none; width:0px'";
				}
				$output .= ">$result[$cname]</td>";
			}
			$output .= "</tr>";
			$k++;
		}
		$output .= "</tbody>";
		$output .= "</table>";

		if ($scrollbar>0) {
			$output .= "</div>";
		}

		return $output;
	}
   
    function db_fetch_val() {
		$result=$this->db_fetch_array();
		return $result[0];
    }

	function db_fetch_array($type='') {
		$result='';

		if ($type==OCI_ASSOC) {
			$result=oci_fetch_array($this->query, OCI_ASSOC+OCI_RETURN_NULLS);
		} else if ($type==OCI_NUM) {
			$result=oci_fetch_array($this->query, OCI_NUM+OCI_RETURN_NULLS);
		} else {
			$result=oci_fetch_array($this->query, OCI_BOTH+OCI_RETURN_NULLS);
		}
	
		if(!is_array($result))
			return false;
		$this->total_field=OCINumCols($this->query);
	    
		foreach($result as $key=>$val) {
//	        $result[$key]=trim($val);
//			$result[$key]=trim(htmlspecialchars($val));
			$result[$key]=htmlspecialchars($val);
	    }
	    $this->rec_position++;
		return $result;
	}

	function get_num_fields() {
		return @ocinumcols($this->query);
	}

	function get_field_name($i) {
		return OCIColumnName($this->query, $i+1);
	}
	
	function get_field_type($i) {
		return ocicolumntype($this->query, $i+1);
	}
	
	function get_field_size($i) {
		return ocicolumnsize($this->query, $i+1);
	}
	
	function total_records() {
		return oci_num_rows($this->query);
	}
	
	function free() { 
		oci_free_statement($this->query); 
		oci_close($this->connection); 
		// unset($this); 
		
	}
	
	function logoff() {
		oci_close($this->connection);
	}
	
	function get_error_msg($error_no, $msg=""){
		$log_msg=NULL;
		$error_msg="<b>Custom Error :</b> <pre><font color=red>\n\t".preg_replace("/,/",",\n\t",$msg)."</font></pre>";
		$error_msg.="<b><i>System generated Error :</i></b>";
		$error_msg.="<font color=red><pre>";
        foreach(ocierror($error_no) as $key=>$val){
			$log_msg.="$key :  ".$val."\n";
			$error_msg.="$key : $val \n";
        }

        $error_msg.="</pre></font>";
        return $error_msg;
	}
	
	function get_error_msg_array($error_no){
		return ocierror($error_no);
	}
}

global $DB;
$DB = new db_connect();
unset($editor_config['ORCL_PASSWORD']);

?>