<?php
// Modifiche
// 15.01,2009 - Gigi - File di log inserito e tolto
// 23.01.2009 - Gigi - Modificata condizione linea 169, in modo da tener conto dei marcatori-riferimento
// 11.05.2010 - Gigi - Gestione dei testi aggiuntivi (linea 136 e segg)
// 14.01.2011 - Gigi - Lancio PP_SCRIVI_TESTO_ANNUNCIO: aggiunti tst, edz e rbraseqid come parametri
// 18.04.2011 - Gigi - Aggiunti commenti alle regexp per calcolo parole
// 24.05.2011 - Gigi - Le ultime 3 righe, che restituiscono l'eventuale errore, sono state portate all'interno del corpo principale
//                     e rese col comando echo; commentata scrittura log
// 20.12.2011 - Gigi - Linea 32: "if (count($query) != 13)" diventa "if (count($query) < 13)" per gestire il parametro fotoobbl
// 18.05.2015 - Gigi - Adattamenti per quantita' tabellare necro
// 14.07.2015 - Gigi - Tolti commenti
// 14.10.2015 - Gigi - Parametrizzato charset di conversione in $editor_config['CHARSET'], settato in ./include/config.inc.php
// 2016.02.03 - Gigi - Commentata scrittura dei log

	require('./include/db_connect.php');
	require_once "./ConvertCharset.class.php";
	
	$targetCharset = "";
	
	if ( $editor_config['CHARSET'] == null OR $editor_config['CHARSET'] == "" ){
   	    $targetCharset = "iso-8859-15";
	} else{
	    $targetCharset = $editor_config['CHARSET'];
	}
//$fp_log=fopen("LOG_SAVETEXT".".txt",'a+');
//fwrite($fp_log, date("Y-m-d G:i:s")." CHARSET ".$targetCharset.chr(13).chr(10) );
//fclose($fp_log);
	
	$NewEncoding = new ConvertCharset("utf-8", $targetCharset, 0);

	// per prima cosa faccio un test se ID mi viene correttamente passato
	// in caso contrario genero una response di errore
	$errore = 0;
	$errore_descrizione = "";
	
	// attenzione le variabili sono passate nel POST
	$id = "";
	if (isset($_POST['id'])) {
	    $id = $_POST['id'];
    }

	// var_dump($id);
	
	if ($id == "")
	{
		$errore = 10000;
		$errore_descrizione = htmlspecialchars("TEXT ID IS UNDEFINED\nThe parent html page does not have and pass parameter: ID");
	}
	
//echo "id ".$id;
	
	$query = explode("|", $id);
	
	if (count($query) < 13)
	{
		$errore = 10001;
		$errore_descrizione = htmlspecialchars("WRONG PARAMETERS NUMBER\nYou pass id=".$id);
	}
	
	if ($errore == 0)
	{
	    if (array_key_exists('FLAGTABELLARE', $query)) {
		  // ora sono sicuro che la chiave 'immatricolazione' esiste...

		// qui le coordinate passate all'inizio in id
		$qs = array (
			'ESERC'      	=> $query[0],
			'SOC' 	     	=> $query[1],
			'UOP' 	     	=> $query[2],
			'FIL' 	     	=> $query[3],
			'NUM' 	     	=> $query[4],
			'ID' 	     	=> $query[5],
			'UTENTE'     	=> $query[6],
			'TST'  	     	=> $query[7],
			'EDZ' 	     	=> $query[8],
			'RUBRSEQ'    	=> $query[9],
			'ATIPA'      	=> $query[10],
			'IMPRESA'    	=> $query[11],
			'TIPOEDITOR' 	=> $query[12],
			'FLAGFOTOOBBL'  => $query[13],
			'FLAGTABELLARE' => $query[14],
			'MODLARGHEZZA'  => $query[15],
			'MODALTEZZA'    => $query[16],
			'MODULI'    	=> $query[17] );
		} else {
		$qs = array (
			'ESERC'      	=> $query[0],
			'SOC' 	     	=> $query[1],
			'UOP' 	     	=> $query[2],
			'FIL' 	     	=> $query[3],
			'NUM' 	     	=> $query[4],
			'ID' 	     	=> $query[5],
			'UTENTE'     	=> $query[6],
			'TST'  	     	=> $query[7],
			'EDZ' 	     	=> $query[8],
			'RUBRSEQ'    	=> $query[9],
			'ATIPA'      	=> $query[10],
			'IMPRESA'    	=> $query[11],
			'TIPOEDITOR' 	=> $query[12],
			'FLAGFOTOOBBL'  => $query[13]);
		}
		
    
//$fp_log=fopen("LOG_SAVETEXT".".txt",'a+');
//fwrite($fp_log, date("Y-m-d G:i:s")." - 18 count ".chr(13).chr(10) );
//for ($g=0;$g<count($params);$g++) {
//  fwrite($fp_log, date("Y-m-d G:i:s")." - PARAM ".$g." - ".$params[$g]." -- ".$params["rbraseqid"].chr(13).chr(10) );
//}
//fclose($fp_log);


		// Nella 14^ posizione, se c'e', c'e' il flag foto obbl.; nella 17^ c'e' il numero di moduli
		if (count($query) == 18){

			$nroModuli = $qs['MODULI'];

//$fp_log=fopen("LOG_SAVETEXT".".txt",'a+');
//fwrite($fp_log, date("Y-m-d G:i:s")." - SALVO IL NUMERO DI MODULI ".chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - $qs nroModuli ".$qs['MODULI'].chr(13).chr(10) );
//fwrite($fp_log, date("Y-m-d G:i:s")." - nroModuli ".$nroModuli.chr(13).chr(10) );
//fclose($fp_log);

//$fp_log=fopen("LOG_SAVETEXT".".txt",'a+');
//fwrite($fp_log, date("Y-m-d G:i:s")." - ESERC: ". $qs['ESERC'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - SOC: ". $qs['SOC'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - UOP: ". $qs['UOP'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - FIL: ". $qs['FIL'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - NUM: ". $qs['NUM'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - ID: ". $qs['ID'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - nroModuli ".$nroModuli.chr(13).chr(10) );
//fwrite($fp_log, date("Y-m-d G:i:s")." - UTENTE: ". $qs['UTENTE'].chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - PGM: ". "savetext.php".chr(13).chr(10));
//fwrite($fp_log, date("Y-m-d G:i:s")." - LINGUA: ". "IT".chr(13).chr(10));
//fclose($fp_log);

			// Lancio il pck per inserire la qta' moduli
			$sql = "BEGIN  PCK_ANNUNCI.PP_SCRIVI_QTA_TAB ".
				   "(:ese, :soc, :uop, :fil, :nmg, :id, :qtaMod, :utente, :pgm, :lingua);".
				   " END;";
			$params = array(
				':ese' 			=> $qs['ESERC'],
				':soc' 			=> $qs['SOC'],
				':uop' 			=> $qs['UOP'],
				':fil' 			=> $qs['FIL'],
				':nmg' 			=> $qs['NUM'],
				':id' 			=> $qs['ID'],
				':qtaMod' 		=> $nroModuli,
				':utente'   	=> $qs['UTENTE'],
				':pgm'          => 'savetext.php',
				':lingua'       => 'IT'
				
			);

			$DB->db_query($sql, $params);
			// var_dump($sql);
		}
		
		// Carica dal DB la tabella col peso delle parole
		$sql = "SELECT APP_ID, APP_PESO
				FROM TANNPESOPAROLE
				WHERE APP_TST_ID = :APP_TST_ID
				AND   APP_EDZ_ID = :APP_EDZ_ID";
		$params = array(
			':APP_TST_ID' => $qs['TST'],
			':APP_EDZ_ID' => $qs['EDZ']
		);
		$DB->db_query($sql, $params);
		while($result = $DB->db_fetch_array(OCI_ASSOC)) {
			$peso_parole[$result['APP_ID']] = $result['APP_PESO'];
		}

		if ($qs['ATIPA']=='') {
			$sql = "SELECT mrc_mrca_id        , mrca_descriz    , mrc_mrca_tp     ,
			       mrc_testo          , mrc_um          , mrc_qta_max     ,
			       mrc_edit_fine_corpo, mrc_pos_video   , mrc_flag_initxt ,
			       mrc_flag_num_alfa  , mrc_ord         , mrc_prompt      ,
			       mrc_flag_upper     , mrc_flag_cr_dopo
			 FROM tmarcatorianag, TMARCATORI
			 WHERE mrc_tst_id      = :tstParam
			   AND mrc_edz_id      = :edzParam
			   AND mrc_rbra_seq_id = :rubrParam 
			   AND mrca_id = mrc_mrca_id
			 ORDER BY mrc_pos_video";
			 
			$params = array(
				':tstParam' => $qs['TST'],
				':edzParam' => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ']
			);
		} else {
			$sql = "SELECT mrc_mrca_id        , mrca_descriz    , mrc_mrca_tp     ,
					       mrc_testo          , mrc_um          , mrc_qta_max     ,
					       mrc_edit_fine_corpo, mrc_pos_video   , mrc_flag_initxt ,
					       mrc_flag_num_alfa  , mrc_ord         , mrc_prompt      ,
					       mrc_flag_upper     , mrc_flag_cr_dopo
					 FROM tmarcatorianag, TMARCATORI, TANNTIPMRC
					 WHERE atipm_soc_id      = :socParam
					   AND atipm_tst_id      = :tstParam
					   AND atipm_edz_id      = :edzParam
					   AND atipm_rbra_seq_id = :rubrParam
					   AND atipm_atipa_id    = :atipParam
					   AND mrc_tst_id        = atipm_tst_id
					   AND mrc_edz_id        = atipm_edz_id
					   AND mrc_rbra_seq_id   = atipm_rbra_seq_id
					   AND mrc_mrca_id       = atipm_mrc_id
					   AND mrca_id           = mrc_mrca_id
					 ORDER BY mrc_pos_video";
			$params = array(
				':socParam' => $qs['SOC'],
				':tstParam' => $qs['TST'],
				':edzParam' => $qs['EDZ'],
				':rubrParam' => $qs['RUBRSEQ'],
				':atipParam' => $qs['ATIPA']
			);
		}

		$marc_tipi = array();
		$DB->db_query($sql, $params);
		while($result = $DB->db_fetch_array(OCI_ASSOC)) {
			$marc_tipi[$result['MRC_MRCA_ID']] = $result;
		}

		$reg_exp = "/(\{([^}]+)\}([^{]*))/";

		// $text contiene il testo vero e proprio
		$text = "";
		$text_before  = "";
		$text_central = "";
		$text_after   = "";
		
		$marc_coll = array();
		$code_coll = array();
		$txt_coll  = array();		
		$peso_coll = array();
		$pos_coll  = array();
				
		if ( $qs['TIPOEDITOR'] == 'CEA' ) {
			
			// Se il tipo editor = CEA, il testo arriva spezzato in 3 parti: PRIMA, centrale, DOPO. Devo salvare questa successione
			// Testo inizio
			if (isset($_POST['text_before'])) 
			{
				$text_before = stripcslashes($_POST['text_before']);
				
				// questa porzione di codice mette a posto il fatto che ho solo \r nel testo tornato da FLEX
				// mentre prima del salvataggio in oracle si vuole \r\n
				$text_before = str_replace("\n", "", $text_before);
				$text_before = str_replace("\r", "\n", $text_before);
				
				// convertiamo
				$text_before = $NewEncoding->Convert($text_before);
			}
			
	 		preg_match_all($reg_exp, $text_before, $mrcs, PREG_PATTERN_ORDER);
			
	 		$marcatori = $mrcs[2];
	 		$testi = $mrcs[3];
//for ($i=0; $i < sizeof($marcatori); $i++) {
//fwrite($fp_log, "marcatore - riga ".$i." - marcatore: ".$marcatori[$i] .chr(13).chr(10) );
//fwrite($fp_log, "testo - riga ".$i." - testo: ".$testi[$i] .chr(13).chr(10) );
//}
			
	 		for($i=0; $i < count($mrcs[1]); $i++) {
	 			$mrc = $marcatori[$i];
	 			$testo = $testi[$i];
	 			
				$mrc = explode("|", $mrc);
				array_push($marc_coll, trim($mrc[0]));
				if (isset($mrc[1]))
					array_push($code_coll, trim($mrc[1]));
				else
					array_push($code_coll, '');
				if (isset($mrc[2]))
					array_push($txt_coll, trim($mrc[2]));
				else
					array_push($txt_coll, $testo);
				
				array_push($pos_coll, 'P');
			}
			
			// Testo centrale
			if (isset($_POST['text_central'])) 
			{
				$text_central = stripcslashes($_POST['text_central']);
				
				// questa porzione di codice mette a posto il fatto che ho solo \r nel testo tornato da FLEX
				// mentre prima del salvataggio in oracle si vuole \r\n
				$text_central = str_replace("\n", "", $text_central);
				$text_central = str_replace("\r", "\n", $text_central);
				
				// convertiamo
				$text_central = $NewEncoding->Convert($text_central);
			}
			
			//Pulisco gli array prima di riutilizzarli
	 		unset( $mrcs );
			$mrcs = array();
	 		preg_match_all($reg_exp, $text_central, $mrcs, PREG_PATTERN_ORDER);
	 		
	 		unset( $marcatori );
			$marcatori = array();
	 		unset( $testi );
			$testi = array();
	 		$marcatori = $mrcs[2];
	 		$testi = $mrcs[3];
//for ($i=0; $i < sizeof($marcatori); $i++) {
//fwrite($fp_log, "marcatore centr - riga ".$i." - marcatore: ".$marcatori[$i] .chr(13).chr(10) );
//fwrite($fp_log, "testo centr - riga ".$i." - testo: ".$testi[$i] .chr(13).chr(10) );
//}
			
	 		for($i=0; $i < count($mrcs[1]); $i++) {
	 			$mrc = $marcatori[$i];
	 			$testo = $testi[$i];
	 			
				$mrc = explode("|", $mrc);
				array_push($marc_coll, trim($mrc[0]));
				if (isset($mrc[1]))
					array_push($code_coll, trim($mrc[1]));
				else
					array_push($code_coll, '');
				if (isset($mrc[2]))
					array_push($txt_coll, trim($mrc[2]));
				else
					array_push($txt_coll, $testo);
				
				array_push($pos_coll, 'C');
			}
			
			// Testo DOPO
			if (isset($_POST['text_after'])) 
			{
				$text_after = stripcslashes($_POST['text_after']);
				
				// questa porzione di codice mette a posto il fatto che ho solo \r nel testo tornato da FLEX
				// mentre prima del salvataggio in oracle si vuole \r\n
				$text_after = str_replace("\n", "", $text_after);
				$text_after = str_replace("\r", "\n", $text_after);
				
				// convertiamo
				$text_after = $NewEncoding->Convert($text_after);
			}
			
			//Pulisco gli array prima di riutilizzarli
	 		unset( $mrcs );
			$mrcs = array();
	 		preg_match_all($reg_exp, $text_after, $mrcs, PREG_PATTERN_ORDER);
	 		
	 		unset( $marcatori );
			$marcatori = array();
	 		unset( $testi );
			$testi = array();
	 		$marcatori = $mrcs[2];
	 		$testi = $mrcs[3];
//for ($i=0; $i < sizeof($marcatori); $i++) {
//fwrite($fp_log, "marcatore centr - riga ".$i." - marcatore: ".$marcatori[$i] .chr(13).chr(10) );
//fwrite($fp_log, "testo centr - riga ".$i." - testo: ".$testi[$i] .chr(13).chr(10) );
//}
			
	 		for($i=0; $i < count($mrcs[1]); $i++) {
	 			$mrc = $marcatori[$i];
	 			$testo = $testi[$i];
	 			
				$mrc = explode("|", $mrc);
				array_push($marc_coll, trim($mrc[0]));
				if (isset($mrc[1]))
					array_push($code_coll, trim($mrc[1]));
				else
					array_push($code_coll, '');
				if (isset($mrc[2]))
					array_push($txt_coll, trim($mrc[2]));
				else
					array_push($txt_coll, $testo);
				
				array_push($pos_coll, 'D');
			}
			
			
		}
		else{
			//  In tutti cli altri tipi di editor, il testo arriva tutto insieme in $_POST['text']
			if (isset($_POST['text'])) 
			{
				$text = stripcslashes($_POST['text']);
				
				// questa porzione di codice mette a posto il fatto che ho solo \r nel testo tornato da FLEX
				// mentre prima del salvataggio in oracle si vuole \r\n
				$text = str_replace("\n", "", $text);
				$text = str_replace("\r", "\n", $text);
				
				// convertiamo
				$text = $NewEncoding->Convert($text);
			}
			
			
	 		preg_match_all($reg_exp, $text, $mrcs, PREG_PATTERN_ORDER);
	 		
	 		$marcatori = $mrcs[2];
	 		$testi = $mrcs[3];
//for ($i=0; $i < sizeof($marcatori); $i++) {
//fwrite($fp_log, "marcatore - riga ".$i." - marcatore: ".$marcatori[$i] .chr(13).chr(10) );
//fwrite($fp_log, "testo - riga ".$i." - testo: ".$testi[$i] .chr(13).chr(10) );
//}
			// QUI APPLICARE RICERCA BOX
		// $riquadrato � una stringa "true" oppure "false"
		$riquadrato = "";
		if (isset($_POST['riquadrato'])){
    		$riquadrato = $_POST['riquadrato'];
//fwrite($fp_log, "testo - riga ".$i." - ISSET RIQ : ".$riquadrato .chr(13).chr(10) );
            if ($riquadrato == "true" ){
			    array_push($marc_coll, "B");
		    	array_push($txt_coll , "BOX" ); // forzo un testo qualsiasi, in modo che venga preso in considerazione in PCK_ANNUNCI
	    		array_push($code_coll, '');
    			array_push($pos_coll, 'C');
			}
		}
			
	 		for($i=0; $i < count($mrcs[1]); $i++) {
	 			$mrc = $marcatori[$i];
	 			$testo = $testi[$i];
	 			
				$mrc = explode("|", $mrc);
				array_push($marc_coll, trim($mrc[0]));
				if (isset($mrc[1]))
					array_push($code_coll, trim($mrc[1]));
				else
					array_push($code_coll, '');
				if (isset($mrc[2]))
					array_push($txt_coll, trim($mrc[2]));
				else
					array_push($txt_coll, $testo);
					
				array_push($pos_coll, 'C');
			}
			
		}
//for ($i=0; $i < sizeof($marc_coll); $i++) {
//fwrite($fp_log, "marcatore/testo - riga ".$i." - marcatore: ".$marc_coll[$i]. " - ".$txt_coll[$i]."  --  ".$pos_coll[$i].chr(13).chr(10) );
//}


		
		$i=0;
		for ($i=0; $i < count($txt_coll); $i++) {
			$row = $txt_coll[$i];
//fwrite($fp_log, "testo - riga ".$i." - testo: $row ".chr(13).chr(10) );
			// Controlla che il marcatore del testo sia di tipo T.
			// In questo caso conta le parole, altrimenti mette peso = 1.
//			if (isset($marc_tipi[$marc_coll[$i]]) && $marc_tipi[$marc_coll[$i]]['MRC_MRCA_TP']=='T') {
			if (isset($marc_tipi[$marc_coll[$i]]) && ( $marc_tipi[$marc_coll[$i]]['MRC_MRCA_TP']=='T' OR $marc_tipi[$marc_coll[$i]]['MRC_MRCA_TP']=='R')  ) {
				// Procedura di conteggio delle parole
				// La riga di testo viene spezzata in un array. Viene generato un nuovo elemento di array se si incontra uno spazio o un apice (la stringa [\s'] significa cio';
				// la parentesi quadra racchiude un elenco di caratteri). Il tutto viene ripetuto per tutte le occorrenze (carattere +)
				$split_words = preg_split("/[\s']+/", $row, -1, PREG_SPLIT_NO_EMPTY);
//for ($g=0;$g<count($split_words);$g++) {
//fwrite($fp_log, "    parole ".$g." - ".$split_words[$g].chr(13).chr(10) );
//}
				$count = 0;
				foreach ($split_words as $word) {
					// Controlla se la parola ha un peso speciale
					if (isset($peso_parole[$word])) {
						$count += $peso_parole[$word];
					} else {
						// Se la parola contiene caratteri diversi
						// da quelli elencati sotto, allora pesa 1
						// Altrimenti se la parola � costituita SOLO
						// da uno o piu' di quei caratteri, pesa 0
						// x21 = !   \x25 = %    \x28 = (    \x29 = )    \x2c = ,  \x2e = .   \x2f = /   \x3a = :   \x3b = ;   \x3f = ?
						// il carattere ^ chiuso tra parentesi quadre e' negazione: se la stringa NON contiene questi caratteri, conto + 1
						if (preg_match("@[^\x21-\x25\x28-\x29\x2c\x2e\x2f\x3a\x3b\x3f]+@", $word))
							$count++;
					}
				}
				array_push($peso_coll, $count);
			} else {
				array_push($peso_coll, 1);
			}
		}
		
		// qui ho il contenuto dei cassetti
		$drawer_1 = "";
		if (isset($_POST['drawer_1'])) $drawer_1 = $_POST['drawer_1'];
		$drawer_1 = $NewEncoding->Convert($drawer_1);
		
		$drawer_2 = "";
		if (isset($_POST['drawer_2'])) $drawer_2 = $_POST['drawer_2'];
		$drawer_2 = $NewEncoding->Convert($drawer_2);
				
		$drawer_3 = "";
		if (isset($_POST['drawer_3'])) $drawer_3 = $_POST['drawer_3'];
		$drawer_3 = $NewEncoding->Convert($drawer_3);
		
		$drawer_4 = "";
		if (isset($_POST['drawer_4'])) $drawer_4 = $_POST['drawer_4'];
		$drawer_4 = $NewEncoding->Convert($drawer_4);
		
		$drawer_5 = "";
		if (isset($_POST['drawer_5'])) $drawer_5 = $_POST['drawer_5'];
		$drawer_5 = $NewEncoding->Convert($drawer_5);
		

		$sql = "BEGIN  PCK_ANNUNCI.PP_SCRIVI_TESTO_ANNUNCIO ".
			   "(:ese, :soc, :uop, :fil, :nmg, :id, :tst, :edz, :rbraseqid, :utente, ".
			   " :tipoeditor, :mrc_coll, :txt_coll, :code_coll, :peso_coll, :pos_coll, :nrec, :drawer1, :drawer2, :drawer3, :drawer4);".
			   " END;";
		$params = array(
			':ese' 			=> $qs['ESERC'],
			':soc' 			=> $qs['SOC'],
			':uop' 			=> $qs['UOP'],
			':fil' 			=> $qs['FIL'],
			':nmg' 			=> $qs['NUM'],
			':id' 			=> $qs['ID'],
			':tst' 			=> $qs['TST'],
			':edz' 			=> $qs['EDZ'],
			':rbraseqid' 	=> $qs['RUBRSEQ'],
			':utente'   	=> $qs['UTENTE'],
			':tipoeditor'   => $qs['TIPOEDITOR'],
			':mrc_coll' 	=> array($marc_coll, -1, SQLT_CHR),
			':txt_coll' 	=> array($txt_coll, -1, SQLT_CHR),
			':code_coll' 	=> array($code_coll, -1, SQLT_CHR),
			':peso_coll' 	=> array($peso_coll, -1, SQLT_INT),
			':pos_coll' 	=> array($pos_coll, -1, SQLT_CHR),
			':nrec'			=> sizeof($marc_coll),
			':drawer1'		=> $drawer_1,
			':drawer2'		=> $drawer_2,
			':drawer3'		=> $drawer_3,
			':drawer4'		=> $drawer_4
		);
//fwrite($fp_log, "    PARAM1 ".$params.chr(13).chr(10) );
//for ($g=0;$g<count($params);$g++) {
//fwrite($fp_log, "    PARAM ".$g." - ".$params[$g]." -- ".$params["rbraseqid"].chr(13).chr(10) );
//}

		$DB->db_query($sql, $params);
//		var_dump($sql);
	}
	
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	echo "<willbiteditor>";
	echo "<error number=\"$errore\" description=\"$errore_descrizione\" />";
    echo "</willbiteditor>";

	
?>
