#!/bin/bash
#---------------------------------------
# Reset
export COLOR_OFF='\e[0m' # Text Reset

# Regular Colors
export BLACK='\e[0;30m'  # Nero
export RED='\e[0;31m'    # Rosso
export GREEN='\e[0;32m'  # Verde
export YELLOW='\e[0;33m' # Giallo
export BLUE='\e[0;34m'   # Blu
export PURPLE='\e[0;35m' # Viola
export CYAN='\e[0;36m'   # Ciano
export WHITE='\e[0;37m'  # Bianco

# Bold
export BBLACK='\e[1;30m'  # Nero
export BRED='\e[1;31m'    # Rosso
export BGREEN='\e[1;32m'  # Verde
export BYELLOW='\e[1;33m' # Giallo
export BBLUE='\e[1;34m'   # Blu
export BPURPLE='\e[1;35m' # Viola
export BCYAN='\e[1;36m'   # Ciano
export BWHITE='\e[1;37m'  # Bianco

# Underline
export UBLACK='\e[4;30m'  # Nero
export URED='\e[4;31m'    # Rosso
export UGREEN='\e[4;32m'  # Verde
export UYELLOW='\e[4;33m' # Giallo
export UBLUE='\e[4;34m'   # Blu
export UPURPLE='\e[4;35m' # Viola
export UCYAN='\e[4;36m'   # Ciano
export UWHITE='\e[4;37m'  # Bianco

# Background
export ON_BLACK='\e[40m'  # Nero
export ON_RED='\e[41m'    # Rosso
export ON_GREEN='\e[42m'  # Verde
export ON_YELLOW='\e[43m' # Giallo
export ON_BLUE='\e[44m'   # Blu
export ON_PURPLE='\e[45m' # PURPLE
export ON_CYAN='\e[46m'   # Ciano
export ON_WHITE='\e[47m'  # Bianco

# High Intensty
export IBLACK='\e[0;90m'  # Nero
export IRED='\e[0;91m'    # Rosso
export IGREEN='\e[0;92m'  # Verde
export IYELLOW='\e[0;93m' # Giallo
export IBLUE='\e[0;94m'   # Blu
export IPURPLE='\e[0;95m' # Viola
export ICYAN='\e[0;96m'   # Ciano
export IWHITE='\e[0;97m'  # Bianco

# Bold High Intensty
export BIBLACK='\e[1;90m'  # Nero
export BIRED='\e[1;91m'    # Rosso
export BIGREEN='\e[1;92m'  # Verde
export BIYELLOW='\e[1;93m' # Giallo
export BIBLUE='\e[1;94m'   # Blu
export BIPURPLE='\e[4;95m' # Viola
export BICYAN='\e[1;96m'   # Ciano
export BIWHITE='\e[1;97m'  # Bianco

# High Intensty backgrounds
export ON_IBLACK='\e[0;100m'  # Nero
export ON_IRED='\e[0;101m'    # Rosso
export ON_IGREEN='\e[0;102m'  # Verde
export ON_IYELLOW='\e[0;103m' # Giallo
export ON_IBLUE='\e[0;104m'   # Blu
export ON_IPURPLE='\e[10;95m' # Viola
export ON_ICYAN='\e[0;106m'   # Ciano
export ON_IWHITE='\e[0;107m'  # Bianco
#---------------------------------------
