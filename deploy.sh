#!/bin/bash
#ver 1.0.0 del 20200709
DIR=./dist
NOW=$(date '+%F_%H:%M:%S')

source colors.sh

clear

printf "\n ${BIBLUE} Current working Directory : ${ON_IGREEN}${BIBLACK} %s ${COLOR_OFF}" "$PWD"
printf "\n ${BIBLUE} Target Directory          : ${ON_IGREEN}${BIBLACK} %s ${COLOR_OFF}" "${DIR}"

printf "\n ${BIPURPLE} Compile TypeScript Source ${COLOR_OFF}\n"
tsc -p ./tsconfig.json
printf "\n ${ON_IGREEN}${BIBLACK} OK ${COLOR_OFF}\n"

printf "\n ${BIPURPLE} Creating Typographic Set ${COLOR_OFF} \n\n"
sass css/main.scss css/main.css
printf "\n ${ON_IGREEN}${BIBLACK} OK ${COLOR_OFF}\n"

printf "\n ${BIRED} Copy SOURCE/MAP ? (Y/N): ${COLOR_OFF}"
read -r -p '' sources

if [[ -d "$DIR" ]]; then
    printf "\n ${BIPURPLE} Removing Target Directory %s ${COLOR_OFF}" "${DIR}"
    rm -rf "$DIR"
    printf "\n\n ${ON_IGREEN}${BIBLACK} OK ${COLOR_OFF}\n"
fi

if [[ -d "$DIR" ]]; then
    printf '%s\n' "Removing dir ($DIR)"
    rm -rf "$DIR"
fi
mkdir $DIR
mkdir $DIR/V1
mkdir $DIR/V1/js
mkdir $DIR/V1/css

cp js/index.js $DIR/V1/js/index.js
cp js/BJSpell.js $DIR/V1/js/BJSpell.js
cp css/main.css $DIR/V1/css/main.css
cp css/tailwind.css $DIR/V1/css/tailwind.css
cp -r img $DIR/V1/img
cp -r fonts $DIR/V1/fonts
cp -r dictionary.js $DIR/V1/dictionary.js
cp index.html $DIR/V1/index.html
cp launcher.html $DIR/V1/launcher.html
cp version.txt $DIR/V1/version.txt

if [[ ${sources} == 'Y' || ${sources} == 'y' ]]; then
    cp js/index.js.map $DIR/V1/js/index.js.map
    cp -r ts $DIR/V1/ts
fi

NOW=$(date '+%F_%H:%M:%S')

printf "\n ${ON_IYELLOW}${BIBLUE} New Version Build into Target Directory ${COLOR_OFF} \n\n  %s" "$PWD"
printf "\n\n ${ON_BLUE}${BIWHITE} %s END PROCESS ${COLOR_OFF}\n\n" "${NOW}"
