/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe di gestione del pannello interattivo su cui l'utente inserisce il testo
	 */
	export class Panel {
		public static lineBreakers = /(\r\n|\n|\r)/gim;
		public static spellChecked = false;

		//Aggiorna il valore dell'ultima posizione del caret all'interno del frame principale
		public static updateCaretPosition(): void {
			Panel.caretOffset = Panel.getCaretCharacterOffsetWithin(Panel.getDOM());
			//console.trace("updateCaretPosition", Panel.caretOffset);
		}

		//Restituisce l'ultima posizione nota del caret o null
		public static getLastCaretPosition(): typeof Panel.caretOffset | null {
			//console.trace("getLastCaretPosition", Panel.caretOffset || null);
			return Panel.caretOffset;
		}

		//Posiziona il cursore ad un offset dato, realativo al frame principale
		public static caretAt(offset: number): void {
			const caretAt = Panel.SetCaretPosition(Panel.getDOM(), offset);
			Panel.caretOffset = caretAt !== -1 ? caretAt : offset;
			//console.trace("caretAt", offset, "PanelOffset", Panel.caretOffset);
		}

		//Posiziona il cursore ad un offset dato, realativo ad un dato elemento
		public static caretWithinAt(el: HTMLElement, offset: number): void {
			Panel.SetCaretPosition(el, offset);
			//console.trace("caretAt", offset, "PanelOffset", Panel.caretOffset);
		}

		//Aggiunge un offset all'ultima posizione nota del caret
		public static addToCaretPosition(n: number): void {
			if (Panel.caretOffset) Panel.caretOffset += n;
			//console.trace("addToCaretPosition", n, Panel.caretOffset);
		}

		//Aggiorna il dato statico che contiene un riferimento all'ultimo elemento su cui
		//l'utente ha avuto una interazione e l'offset all'interno dello stesso
		private static updateLastCaretElement() {
			const target = Panel.getCursorElement();
			const inside = Panel.getDOM().contains(target);
			if (target && inside) {
				Panel.LCE = {
					element: target,
					offset: Panel.getCaretCharacterOffsetWithin(target),
				};
			}
		}

		//riferimento statico all'ultimo elemento con cui l'utente ha interagito ed al suo valore di offset
		public static LCE: {
			element: HTMLElement | null;
			offset: number | null;
		} = {
			element: null,
			offset: null,
		};
		private current: string;
		constructor(private dom: HTMLElement, private original: string) {
			this.current = this.original;
			this.contentFromText();
			this.dom.addEventListener("mouseup", () => {
				Panel.updateCaretPosition();
				Panel.updateLastCaretElement();
			});
			this.dom.addEventListener("keyup", () => {
				Panel.updateCaretPosition();
				Panel.updateLastCaretElement();
			});
			if (original.trim() !== "") Panel.setCaretToEnd();
			else Panel.caretOffset = 0;
			const editorDOM = Panel.getDOM();
			editorDOM.addEventListener("paste", Panel.handlePaste);
			editorDOM.focus();
		}

		private static handlePaste(event: ClipboardEvent) {
			let paste = (event.clipboardData || (window as any).clipboardData).getData("text");
			paste = Panel.strip(paste);
			const selection = window.getSelection();
			if (selection && !selection.rangeCount) return false;
			else if (selection) {
				selection.deleteFromDocument();
				Panel.cleanDom();
				const lines = (paste as string).split(Panel.lineBreakers);
				let prev: HTMLElement = {} as HTMLElement;
				for (let i = 0; i < lines.length; i++) {
					const l = lines[i];
					if (l.trim() !== "" && l.trim() !== "\n") {
						const node = document.createElement("div");
						node.innerHTML = l.trim();
						if (i === 0) {
							selection.getRangeAt(0).insertNode(node);
						} else if (prev) prev?.parentNode?.insertBefore(node, prev.nextSibling);
						prev = node;
						event.preventDefault();
						if (i === lines.length - 1) {
							Panel.setCaretToEndWithin(node);
							Panel.LCE.element = node;
							Panel.LCE.offset = node.textContent?.length || 0;
						}
					}
				}
				selection.removeAllRanges();
				Panel.updateLastCaretElement();
			}
		}

		private static caretOffset: number | null = null;

		//Restituisce il DOM del Panel
		public static getDOM = (): HTMLElement => document.getElementById("editor") as HTMLElement;

		//Crea il contenuto del Panel dai dati testuali attuali
		private contentFromText() {
			const dom = Panel.getDOM();
			dom.innerHTML = "";
			const lines = this.current.split(Panel.lineBreakers);
			lines.forEach((l) => {
				if (l.trim() !== "") {
					const div = document.createElement("DIV");
					div.innerHTML = l;
					dom.appendChild(div);
				}
			});
		}

		private static cleanDom() {
			const dom = Panel.getDOM();
			const emptyAnchors = dom.querySelectorAll("div:empty") as NodeListOf<HTMLElement>;
			emptyAnchors.forEach((d) => {
				d.parentNode?.removeChild(d);
			});
		}

		//Crea il dato testuale dal contenuto attuale del Panel
		private textFromContent() {
			const dom = Panel.getDOM();
			const nodes = dom.childNodes;
			let text = "";
			nodes.forEach((element) => {
				if (element.textContent && element.textContent?.trim() !== "") text += Panel.strip(element.textContent) + "\n";
			});
			this.current = text;
		}

		// restituisce, se esiste, il testo selezionato ed il suo contenitore, altrimenti restituisce null
		public static getSelection(): { text: string; elem: HTMLElement } | null {
			const selection = window.getSelection();
			if (selection && selection.toString() !== "")
				return {
					text: selection.toString(),
					elem: selection.anchorNode?.parentNode as HTMLElement,
				};
			else return null;
		}

		//sostituisce la porzione di testo selezionato con un testo dato
		public static replaceSelectedText(replacementText: string): void {
			let sel, range;
			if (window.getSelection) {
				sel = window.getSelection();
				if (sel && sel.rangeCount) {
					range = sel.getRangeAt(0);
					range.deleteContents();
					range.insertNode(document.createTextNode(replacementText));
					sel.removeAllRanges();
				}
			}
		}

		//Attiva il controllo ortografico
		public spellcheck(): void {
			if (!Panel.spellChecked) {
				Panel.spellChecked = true;
				const text = stripBlankLines(Marker.stripMarkers(this.getCurrentText()));
				const words = text.match(/(?<!\S)[A-Za-z]+(?!\S)/g);
				if (words)
					words.forEach((w) => {
						const correct = SpellChecker.check(w);
						if (!correct) {
							this.dom.childNodes.forEach((el) => {
								const elem = el as HTMLElement;
								elem.innerHTML = elem.innerHTML.replace(w, `<span id="${w}" class="underline">${w}</span>`);
							});
							const spans = this.dom.getElementsByClassName("underline");
							for (const s of spans) {
								const span = s as HTMLElement;
								const mouseOver = () => {
									let timer = 0;
									const removeIt = () => {
										span.removeEventListener("mouseleave", mouseOut);
										const tip = document.body.getElementsByClassName("tooltip-text").item(0);
										if (tip) document.body.removeChild(tip);
									};
									const mouseOut = () => (timer = setTimeout(removeIt, 500));
									const ul = document.createElement("ul");
									ul.className = "tooltip-text";
									const wrd = span.textContent;
									if (wrd)
										for (let i = 0, suggest = SpellChecker.suggest(wrd, 6); i < 6; i++) {
											const li = document.createElement("li");
											li.classList.add("hover:bg-blue-500", "hover:text-white", "cursor-pointer", "p-2");
											li.appendChild(document.createTextNode(suggest[i]));
											li.addEventListener("click", () => {
												this.replaceWord(wrd, suggest[i]);
												Editor.resetSpellCheck();
												this.spellcheck();
											});
											ul.appendChild(li);
										}
									ul.addEventListener("mouseenter", () => {
										clearTimeout(timer);
										ul.addEventListener("mouseleave", mouseOut);
									});
									Tips.eraseTips();
									Tips.computeGeoStyle(span as HTMLElement, ul);
								};
								span.addEventListener("mouseover", mouseOver, false);
							}
						}
					});
			} else this.uncheckSpell();
			Editor.updateSpellCheckIcon(Panel.spellChecked);
		}

		//Disattiva il controllo ortografico
		public uncheckSpell(): void {
			if (Panel.spellChecked) {
				Panel.spellChecked = false;
				Tips.eraseTips();

				const checked = this.dom.getElementsByClassName("underline");
				[...checked].forEach((span) => {
					const w = span.textContent;
					this.dom.innerHTML = this.dom.innerHTML.replace(span.outerHTML, w as string);
				});
			}
			Editor.updateSpellCheckIcon(Panel.spellChecked);
		}

		//Sostituisce una stringa con una data nel contenuto testuale
		public replaceWord(oldW: string, newW: string): void {
			const text = this.getCurrentText();
			this.current = text.replace(oldW, newW);
			this.contentFromText();
		}

		//Restituisce l'elemento in cui si trova il caret attualmente, o null
		public static getCursorElement(): HTMLElement | null {
			const sel = window.getSelection();
			if (sel && sel.rangeCount > 0) {
				const range = sel.getRangeAt(0);
				const pointerDiv = range.startContainer;
				return pointerDiv.nodeType === 3 ? (pointerDiv.parentElement as HTMLElement) : (pointerDiv as HTMLElement);
			}
			return null;
		}

		//Inserisce del testo all'ultima posizione nota del caret
		public insertTextAtCaret(text: string): void {
			if (window.getSelection) {
				const sel = window.getSelection();
				if (sel && sel.getRangeAt && sel.rangeCount) {
					const range = sel.getRangeAt(0);
					range.deleteContents();
					const target = document.createTextNode(text);
					range.insertNode(target);
					range.selectNodeContents(target);
					range.collapse(false);
					sel.removeAllRanges();
					sel.addRange(range);
					range.detach();
				}
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
			} else if ((document as any).selection && (document as any).selection.createRange) {
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				(document as any).selection.createRange().text = text;
			}
			this.textFromContent();
		}

		//restituisce o crea e poi restituisce un elemento HTML nel DOM del Panel
		public static getOrCreateLastTextElement(): HTMLElement {
			const last = Panel.getLastTextElement();
			if (last) return last;
			else return Panel.createLastTextElement();
		}

		//Restituisce, se esiste l'ultimo elemento HTML del Panel
		public static getLastTextElement(): HTMLElement | null {
			const dom = Panel.getDOM();
			const last = dom.lastElementChild as HTMLElement;
			if (last && last.tagName === "BR") {
				last.parentNode?.removeChild(last);
				return null;
			}
			return last;
		}

		//Crea al fondo del Panel un elemento vuoto e lo restituisce
		public static createLastTextElement(): HTMLElement {
			const dom = Panel.getDOM();
			const div = document.createElement("DIV");
			dom.appendChild(div);
			return div;
		}

		//Inserisce un dato HTML immediatamente dopo un altro HTML
		public static insertAfter(newNode: HTMLElement, referenceNode: HTMLElement): void {
			referenceNode.parentNode?.insertBefore(newNode, referenceNode.nextSibling);
		}

		//Restituisce il contenuto testuale attuale
		public getCurrentText(): string {
			return this.current;
		}

		//Flag su testo modificato rispetto all'originale
		public contentChanged(): boolean {
			return this.current !== this.original;
		}

		//Metodo esposto per creare il dato testuale dal contenuto attuale del Panel
		public update(): void {
			this.textFromContent();
		}

		//Aggiunge un Marker alla fine del Panel
		public appendTag(tag: ITag): void {
			Panel.getOrCreateLastTextElement().innerHTML += tag.text && tag.text !== "" ? `{${tag.data}}${tag.text}` : `{${tag.data}}`;
			this.textFromContent();
		}

		public appendDatePlusOne(dateString: string): void {
			Panel.getOrCreateLastTextElement().innerHTML += dateString;
			this.textFromContent();
		}

		//Aggiunge testo ad un elemento dentro il Panel
		public appendTextToElem(text: string, elem: HTMLElement): void {
			elem.innerHTML += `${text}`;
			this.textFromContent();
		}

		//Aggiunge una FOTO alla fine del Panel
		public appendPhoto(image: IImage): void {
			const last = Panel.getOrCreateLastTextElement();
			last.innerHTML += `{${image.data}}`;
			this.textFromContent();
		}

		//Ripristina il contenuto originale dell'annuncio nel Panel, scartando tutte le modifiche
		public reset(): void {
			this.current = this.original;
			this.contentFromText();
			//console.trace("reset");
			Panel.caretOffset = 0;
		}

		//Restituisce l'offset attuale del caret rispetto al frame principale
		public static getCaretPosition(): number | null {
			if (window.getSelection && window.getSelection()?.getRangeAt) {
				const selectedObj = window.getSelection();
				if (selectedObj && selectedObj.rangeCount > 0) {
					const range = selectedObj.getRangeAt(0);
					let rangeCount = 0;
					const childNodes = selectedObj?.anchorNode?.parentNode?.childNodes;
					if (childNodes && range) {
						for (let i = 0; i < childNodes.length; i++) {
							if (childNodes[i] == selectedObj?.anchorNode) {
								break;
							}
							if ((childNodes[i] as Element).outerHTML) rangeCount += (childNodes[i] as Element).outerHTML.length;
							else if (childNodes[i].nodeType == 3) {
								rangeCount += (childNodes[i] as any).textContent?.length;
							}
						}
						return range.startOffset + rangeCount;
					}
				}
			}
			return null;
		}

		public static strip(html: string): string {
			const doc = new DOMParser().parseFromString(html, "text/html");
			return (
				(doc.body.textContent as string)
					.replace(new RegExp(String.fromCharCode(160), "g"), " ")
					.replace(/`/g, "'")
					.replace(/’/g, "'")
					.replace(new RegExp(String.fromCharCode(96), "g"), String.fromCharCode(39))
					.replace(new RegExp(String.fromCharCode(180), "g"), String.fromCharCode(39))
					.replace(new RegExp(String.fromCharCode(146), "g"), String.fromCharCode(39)) || ""
			);
		}

		//Restituisce l'offset attuale del caret rispetto ad un elemento dato
		public static getCaretCharacterOffsetWithin(element: HTMLElement): number {
			let caretOffset = 0;
			const doc = element.ownerDocument || (element as any).document;
			const win = doc.defaultView || (doc as any).parentWindow;
			let sel;
			if (win && typeof win.getSelection !== "undefined") {
				sel = win.getSelection();
				if (sel && sel.rangeCount > 0) {
					const range = win.getSelection()?.getRangeAt(0);

					if (range) {
						const preCaretRange = range?.cloneRange();
						preCaretRange.selectNodeContents(element);
						preCaretRange.setEnd(range.endContainer, range.endOffset);
						caretOffset = preCaretRange.toString().length;
					}
				}
			} else if ((sel = (doc as any).selection) && sel.type != "Control") {
				const textRange = sel.createRange();
				const preCaretTextRange = (doc.body as any).createTextRange();
				preCaretTextRange.moveToElementText(element);
				preCaretTextRange.setEndPoint("EndToEnd", textRange);
				caretOffset = preCaretTextRange.text.length;
			}
			return caretOffset;
		}

		//Posiziona il caret alla fine del Panel
		public static setCaretToEnd(): void {
			const dom = Panel.getDOM();
			const target = dom.lastElementChild as HTMLElement;
			Panel.setCaretToEndWithin(target);
		}

		//Posiziona il caret alla fine di un elemento
		public static setCaretToEndWithin(target: HTMLElement): void {
			const range = document.createRange();
			const sel = window.getSelection();
			if (target) {
				range.selectNodeContents(target);
				range.collapse(false);
				if (sel) {
					sel.removeAllRanges();
					sel.addRange(range);
					target.focus();
					range.detach();
					Panel.updateCaretPosition();
					//console.trace("setCaretToEndWithin", Panel.caretOffset);
				}
			}
		}

		//Posiziona il caret all'inizio di un elemento
		public static setCaretToStartWithin(target: HTMLElement): void {
			const range = document.createRange();
			const sel = window.getSelection();
			range.selectNodeContents(target);
			range.collapse(false);
			if (sel) {
				sel.removeAllRanges();
				sel.addRange(range);
				target.focus();
				range.detach();
				Panel.updateCaretPosition();
				//console.trace("setCaretToStartWithin", Panel.caretOffset);
			}
		}

		//Posiziona il caret ad un offset specifico all'interno di un elemento dato
		private static SetCaretPosition(el: Node, pos: number): number {
			for (const node of el.childNodes) {
				if (node.nodeType == 3 && node.nodeValue) {
					if (node.nodeValue.length >= pos) {
						const range = document.createRange(),
							sel = window.getSelection();
						range.setStart(node, pos);
						range.collapse(true);
						if (sel && sel.rangeCount > 0) {
							sel.removeAllRanges();
							sel.addRange(range);
							return -1; // we are done
						}
					} else {
						pos -= node.nodeValue.length;
					}
				} else {
					pos = Panel.SetCaretPosition(node, pos);
					if (pos == -1) {
						return -1; // no need to finish the for loop
					}
				}
			}
			return pos;
		}
	}
}
