/**
 *
 * Varie funzioni di comodo per la gestione di HTML e stringhe
 */

// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	export function textCounter(
		text: string
	): {
		charactersNoSpaces: number;
		characters: number;
		words: number;
		lines: number;
	} {
		text = Marker.stripMarkers(text);
		const w = text.match(/\S+/g);
		return {
			charactersNoSpaces: text.replace(/\s+/g, "").length,
			characters: text.length,
			words: w ? w.length : 0,
			lines: text.split(/\r*\n/).length,
		};
	}

	export function wordCounter(text: string): number {
		return text.split(" ").filter(Boolean).length;
	}

	export function stripBlankLines(text: string): string {
		return text.replace(/^\s*$(?:\r\n?|\n)/gm, "");
	}

	export function isAlphaDigit(key: string): boolean {
		return /^[A-Za-z]+$/.test(key);
	}

	export function escapeRegExp(string: string): string {
		return string.replace(/[.*+\-?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
	}

	export function replaceAll(str: string, find: string, replace: string): string {
		return str.replace(new RegExp(escapeRegExp(find), "g"), replace);
	}

	export class TextObserver {
		private config: MutationObserverInit = { attributes: false, childList: true, characterData: true, subtree: true };

		constructor(targetNode: HTMLElement, cb: (mutations: MutationRecord[]) => void) {
			const observer = new MutationObserver(cb);
			observer.observe(targetNode, this.config);
			//this.observer.disconnect();
		}
	}

	export function softTrim(str: string): string {
		for (let index = 0; index < str.length; index++) {
			const char = str[index];
			if (char === " ") str = str.slice(0, 1);
			else break;
		}
		return str;
	}

	/**
	 * Da stringa a DOM Object
	 * @param {string} htmlString
	 * @return {HTMLElement}
	 */
	export function createElementFromHTML(htmlString: string): HTMLElement {
		const div = document.createElement("div");
		div.innerHTML = htmlString.trim();
		return div.firstElementChild as HTMLElement;
	}

	export class Shortcut {
		private static available = [
			"Digit1",
			"Digit2",
			"Digit3",
			"Digit4",
			"Digit5",
			"Digit6",
			"Digit7",
			"Digit8",
			"Digit9",
			"Digit0",
			"KeyW",
			"KeyE",
			"KeyT",
			"KeyY",
			"KeyU",
			"KeyI",
			"KeyO",
			"KeyP",
			"KeyA",
			"KeyF",
		];
		private static used: string[] = [];
		private static tb: Shortcut[] = [];
		private static targetRelative = "relative";
		public static isLighting = false;

		private key = "";
		private dom;
		constructor(private target: HTMLElement, private cb: () => void, key?: string) {
			this.key = key ? key : Shortcut.getNextAvailable() || "";
			if (!this.key) Logger.log("Max shortcut reached", "ERROR");
			else {
				const div = document.createElement("DIV");
				div.innerHTML = this.template();
				this.dom = div.firstElementChild as HTMLElement;
				Shortcut.tb.push(this);
				Shortcut.used.push(this.key);
			}
		}

		private template = () =>
			`<div class="h-full w-full bg-green-500 text-white text-2xl font-black pr-2 flex items-center justify-end" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0; opacity: 0.5;">${this.key.charAt(
				this.key.length - 1
			)}</div>`;

		private static getNextAvailable(): string | undefined {
			for (const s of Shortcut.available) if (Shortcut.used.indexOf(s) === -1) return s;
		}

		public static call(shortcut: string): void {
			if (Shortcut.isUsed(shortcut)) {
				Shortcut.tb.filter((s) => s.key == shortcut)[0].cb();
			}
		}

		private static isUsed(shortcut: string): boolean {
			return Shortcut.used.indexOf(shortcut) !== -1;
		}

		public static highlight(): void {
			Shortcut.isLighting = true;
			Shortcut.tb.forEach((s) => {
				s.target.classList.add(Shortcut.targetRelative);
				if (s.dom) s.target.appendChild(s.dom);
			});
		}

		public static lightOff(): void {
			Shortcut.isLighting = false;
			Shortcut.tb.forEach((s) => {
				s.target.classList.remove(Shortcut.targetRelative);
				if (s.dom) s.dom.parentNode?.removeChild(s.dom);
			});
		}
	}
}
