// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 *
	 * Classe di gestione dei Marker
	 */
	export class Marker {
		private static tb: Marker[] = [];
		public static curlyHunterRegex = /{([^}]*)}/gm;
		public static getAll(): Marker[] {
			return Marker.tb;
		}
		private template = () => `<div class="marker w-full text-blue-400 hover:bg-blue-400 hover:text-white text-left py-2 pl-2 border border-grey-500 cursor-pointer">
            ${this.tag.label}
			</div>`;

		public shortcut: string;
		private dom: HTMLElement;
		constructor(private container: HTMLElement, public tag: ITag) {
			const div = document.createElement("DIV");
			div.innerHTML = this.template();
			this.dom = div.firstElementChild as HTMLElement;
			this.dom.addEventListener("mousedown", (e) => {
				e.preventDefault();
				e.stopImmediatePropagation();
				e.stopPropagation();
				Editor.addMarker(this.tag);
			});
			this.container.appendChild(this.dom);
			this.shortcut = this.tag.label.split("-")[0].trim();
			Logger.log(`Caricato Marker: {${this.tag.data}}, shortcut: ${this.shortcut}`);
			Marker.tb.push(this);
		}

		public static hasMaxWords(tag: ITag): boolean | number {
			return tag.maxwords && tag.maxwords !== "" ? Number(tag.maxwords) : false;
		}

		public static stripMarkers(text: string): string {
			return text.replace(Marker.curlyHunterRegex, "");
		}

		public static getMarkerByShortcut(shortcut: string): Marker | null {
			const mk = Marker.getAll().filter((m) => m.shortcut === shortcut);
			return mk.length > 0 ? mk[0] : null;
		}

		public static getTagByData(data: string): ITag | null {
			const mk = Marker.getAll().filter((m) => m.tag.data === data);
			return mk.length > 0 ? mk[0].tag : null;
		}

		public static getActiveMarker(target: HTMLElement): ITag | null {
			if (target.textContent) {
				const curlyContent = target.textContent.match(Marker.curlyHunterRegex);
				if (curlyContent && curlyContent.length > 0 && !Simbol.isSimbol(curlyContent[0].trim().slice(1, -1))) {
					const leftText = target.textContent.substring(0, Panel.getLastCaretPosition() || target.textContent.length);
					const leftCurly = leftText.match(Marker.curlyHunterRegex);
					if (leftCurly && leftCurly.length > 0) return Marker.getTagByData(leftCurly[leftCurly.length - 1].trim().slice(1, -1));
					else return Marker.getTagByData(curlyContent[0].trim().slice(1, -1));
				} else if (target.previousElementSibling) {
					return Marker.getActiveMarker(target.previousElementSibling as HTMLElement);
				} else return null;
			} else if (target.previousElementSibling) {
				return Marker.getActiveMarker(target.previousElementSibling as HTMLElement);
			} else return null;
		}
	}
}
