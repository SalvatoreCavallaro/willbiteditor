// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 *
	 * Classe di gestione delle FOTO
	 */
	export class Picture {
		private static tb: Picture[] = [];
		public static srcURl = `https://192.168.167.13/Fotonecro/`;
		public static getAll(): Picture[] {
			return Picture.tb;
		}
		private template = () => `<div class="w-full text-blue-400 hover:bg-blue-400 hover:text-white text-left py-4 pl-2 border border-grey-500 cursor-pointer">
            ${this.image.label}
			</div>`;

		private dom: HTMLElement;
		constructor(private container: HTMLElement, public image: IImage) {
			const div = document.createElement("DIV");
			div.innerHTML = this.template();
			this.dom = div.firstElementChild as HTMLElement;
			this.dom.addEventListener("mousedown", (e) => {
				e.preventDefault();
				e.stopImmediatePropagation();
				e.stopPropagation();
				Editor.addPhoto(this.image);
			});
			this.container.appendChild(this.dom);
			Logger.log(`Caricato Immagine: {${this.image.label}}`);
			new Shortcut(this.dom, () => {
				this.shortCut();
			});
			Picture.tb.push(this);
		}

		private shortCut() {
			Editor.addPhoto(this.image);
		}

		public static isFoto(curlyContent: string): boolean {
			return curlyContent.split("|").length >= 2;
		}

		public static getByData(data: string): Picture | null {
			return Picture.tb.filter((i) => i.image.data === data)[0] || null;
		}
	}
}
