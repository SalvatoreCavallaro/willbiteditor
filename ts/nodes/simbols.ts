// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 *
	 * Classe di gestione dei Simbols
	 */
	export class Simbol {
		private static tb: Simbol[] = [];
		public static getAll(): Simbol[] {
			return Simbol.tb;
		}
		private template = () => `<div class="w-16 h-10 m-2 pr-6 bg-white text-blue-400 hover:bg-blue-400 hover:text-white rounded border border-grey-500 cursor-pointer flex items-center justify-center text-base">
            ${this.simbol.label}
			</div>`;
		private dom: HTMLElement;
		constructor(
			private container: HTMLElement,
			private simbol: {
				label: string;
				data: string;
			}
		) {
			const div = document.createElement("DIV");
			div.innerHTML = this.template();
			this.dom = div.firstElementChild as HTMLElement;
			this.container.appendChild(this.dom);
			this.dom.addEventListener("click", () => {
				Editor.addSimbol(simbol);
			});
			new Shortcut(this.dom, () => {
				Editor.addSimbol(simbol);
			});
			Logger.log(`Caricato Symbol: {${this.simbol.label}}`);
			Simbol.tb.push(this);
		}

		public static isSimbol(data: string): boolean {
			Simbol.tb.forEach((s) => {
				if (data === s.simbol.data) {
					return true;
				}
			});
			return false;
		}
	}
}
