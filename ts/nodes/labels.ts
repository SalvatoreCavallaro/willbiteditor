// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 *
	 * Classe di gestione dei Simbols
	 */
	export class Label {
		private template = () => `<div class="w-full h-10 p-2 bg-white text-blue-400 hover:bg-blue-400 hover:text-white rounded border border-grey-500 cursor-pointer flex items-center justify-center text-base text-center font-semibold">
            ${this.label}
			</div>`;
		private dom: HTMLElement;
		constructor(private container: HTMLElement, private label: string) {
			const div = document.createElement("DIV");
			div.innerHTML = this.template();
			this.dom = div.firstElementChild as HTMLElement;
			this.container.appendChild(this.dom);
			this.dom.addEventListener("click", () => {
				Editor.addLabel(label);
			});
			new Shortcut(this.dom, () => {
				Editor.addLabel(label);
			});
			Logger.log(`Caricato LabelName: {${this.label}}`);
		}
	}
}
