// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe che rappresenta l'oggetto Drawer,
	 */
	export class Drawer {
		/** Tabella statica dei drawer  */
		private static tb: Drawer[] = [];
		private static emptyClass = "text-center text-gray-500 bg-gray-50 hover:bg-blue-400 hover:text-white";
		private static fullClass = "pl-4 text-left text-blue-400";
		private static emptyString = "EMPTY";
		public static getAll(): Drawer[] {
			return Drawer.tb;
		}

		//Template HMTL per il Drawer
		private template = () => `<div class="flex border border-grey-500">
										<div class="drawer-btn p-2 rounded border border-blue-400 text-blue-400 hover:bg-blue-400 hover:text-white cursor-pointer flex justify-center items-center ${
											this.text === "EMPTY" ? "hidden" : ""
										}">Type</div>
										<div class="drawer-label flex-grow  py-4 cursor-pointer ${this.text === "EMPTY" ? Drawer.emptyClass : Drawer.fullClass}">
            								${this.text}
										</div>
									</div>`;

		private dom: HTMLElement;
		private label: HTMLElement;
		private btn: HTMLElement;
		private editMode = false;
		constructor(private container: HTMLElement, private text: string) {
			const div = document.createElement("DIV");
			div.innerHTML = this.template();
			this.dom = div.firstElementChild as HTMLElement;
			this.label = this.dom.getElementsByClassName("drawer-label").item(0) as HTMLElement;
			this.btn = this.dom.getElementsByClassName("drawer-btn").item(0) as HTMLElement;
			this.container.appendChild(this.dom);
			if (this.text !== Drawer.emptyString) Logger.log(`Caricato Drawer: {${this.text}}`);
			Drawer.tb.push(this);
			this.label.addEventListener("click", () => {
				this.handleClick();
			});
			new Shortcut(this.dom, () => {
				this.shortCut();
			});
			if (this.text !== Drawer.emptyString) this.btn.addEventListener("click", () => this.textAddRequest());
		}

		//Restituisce il testo del Drawer se non vuoto
		public getText(): string | null {
			return this.text !== Drawer.emptyString ? this.text : null;
		}

		//Gestione del click su Drawer
		private handleClick() {
			if (!this.editMode) {
				this.editMode = true;
				if (this.text === Drawer.emptyString) {
					this.label.classList.remove(...Drawer.emptyClass.split(" "));
					this.label.classList.add(...Drawer.fullClass.split(" "));
					this.label.textContent = "";
				}
				this.label.setAttribute("contenteditable", "true");
				this.label.focus();
				const enter = (e: KeyboardEvent) => {
					if (e.key === "Enter") {
						e.preventDefault();
						this.label.blur();
					}
				};
				const blur = () => {
					this.editMode = false;
					this.text = this.label.textContent?.trim() || Drawer.emptyString;
					if (this.text === Drawer.emptyString) {
						this.label.classList.remove(...Drawer.fullClass.split(" "));
						this.label.classList.add(...Drawer.emptyClass.split(" "));
						this.btn.classList.add("hidden");
						this.btn.removeEventListener("click", this.textAddRequest);
					} else {
						if (this.btn.classList.contains("hidden")) this.btn.addEventListener("click", () => this.textAddRequest());
						this.btn.classList.remove("hidden");
					}
					this.label.textContent = this.text;
					this.label.removeEventListener("keydown", enter);
					this.label.removeEventListener("blur", blur);
				};
				this.label.addEventListener("keydown", enter);
				this.label.addEventListener("blur", blur);
			}
		}

		//Aggiunge testo su Panel
		private textAddRequest() {
			if (this.label && this.label.textContent) Editor.addDrawerText(this.label.textContent.trim());
		}

		//Attiva il Drawer tramite shortCut
		private shortCut() {
			if (this.text === Drawer.emptyString) {
				this.handleClick();
			} else this.textAddRequest();
		}
	}
}
