// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	export type IQuadr = "top-left" | "top-right" | "bottom-left" | "bottom-right";

	export interface ICoord {
		quad: IQuadr;
		coord: DOMRect;
	}

	/**
	 * Classe che pozioona nel punto giusto il tooltip con i suggerimenti del controllo ortografico
	 */
	export class Tips {
		public static computeGeoStyle(itm: HTMLElement, tip: HTMLElement): void {
			const geo = Tips.getCoord(itm);
			tip.classList.add(geo.quad);
			document.body.appendChild(tip);
			switch (geo.quad) {
				case "bottom-left":
					tip.style.top = `${geo.coord.top - tip.offsetHeight - 10}px`;
					tip.style.left = `${geo.coord.right - geo.coord.width / 2 - 15}px`;
					break;
				case "bottom-right":
					tip.style.top = `${geo.coord.top - tip.offsetHeight - 10}px`;
					tip.style.left = `${geo.coord.left - tip.offsetWidth + geo.coord.width / 2 + 15}px`;
					break;
				case "top-left":
					tip.style.top = `${geo.coord.bottom + 10}px`;
					tip.style.left = `${geo.coord.right - geo.coord.width / 2 - 15}px`;
					break;
				case "top-right":
					tip.style.top = `${geo.coord.bottom + 10}px`;
					tip.style.left = `${geo.coord.left - tip.offsetWidth + geo.coord.width / 2 + 15}px`;
					break;
			}
		}

		private static getCoord(el: HTMLElement): ICoord {
			const vw = document.body.getBoundingClientRect().width;
			const vh = document.body.getBoundingClientRect().height;
			const coord = el.getBoundingClientRect() as DOMRect;
			const st = coord.top < vh / 2 ? "top" : "bottom";
			const nd = coord.left < vw / 2 ? "left" : "right";
			const quad = (st + "-" + nd) as IQuadr;
			return { quad, coord };
		}

		public static tooltipDom(txt: string): HTMLElement {
			return createElementFromHTML(`<span class="tooltip-text px-2">${txt}</span>`);
		}

		public static eraseTips(): void {
			const tips = document.getElementsByClassName("tooltip-text");
			for (const t of tips) t.parentNode?.removeChild(t);
		}
	}
}
