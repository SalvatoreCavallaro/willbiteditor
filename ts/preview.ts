// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe di gestione del pannello Preview
	 *
	 * Associa ad ogni Marker un tag HTML che contiene una classe specifica
	 *
	 * Le classi CSS rispecchiano la formattazione contenuta nell'XML
	 */
	export class Preview {
		private closeTags: string[] = [];
		public static spaceChecked = false;
		private static spaceStringElem = `<span class="bg-red-500"> </span>`;

		constructor(private dom: HTMLElement) {}

		public spacecheck(): void {
			if (!Preview.spaceChecked) {
				Preview.spaceChecked = true;
				const child = this.dom.children;
				for (const c of child) {
					const elem = c as HTMLElement;
					if (!elem.classList.contains("FOTO")) elem.innerHTML = elem.innerHTML.replace(/\s{2,}/g, Preview.spaceStringElem);
				}
			} else this.resetSpace();
			Editor.updateSpaceCheckIcon(Preview.spaceChecked);
		}

		public resetSpace(): void {
			if (Preview.spaceChecked) {
				Preview.spaceChecked = false;
				const child = this.dom.children;
				for (const c of child) {
					const elem = c as HTMLElement;
					elem.innerHTML = replaceAll(elem.innerHTML, Preview.spaceStringElem, "  ");
				}
			}
		}

		public update(): void {
			const text = Editor.getCurrentText();
			const lines = text.split(Panel.lineBreakers);
			let html = "";
			let img: Picture | null = null;
			lines.forEach((l) => {
				if (l.trim() !== "") {
					const curlyContent = l.match(Marker.curlyHunterRegex);
					if (curlyContent && curlyContent.length > 0) {
						// CI SONO TAG NELLA RIGA
						if (curlyContent.length === 1) {
							const curlyString = curlyContent[0].trim().slice(1, -1);
							const tag = Marker.getTagByData(curlyString);
							if (tag) {
								if (this.closeTags.length > 0) {
									for (let i = 0; i < this.closeTags.length; i++) {
										html += this.closeTags[i];
									}
									this.closeTags = [];
								}
								if (tag.text === "CROCE") {
									if (tag.format_start.indexOf("<") !== -1) html += tag.format_start + tag.format_end;
									else if (tag.format_end === "") html += `<p class="CROCE">&#8224;<p>`;
								} else html += this.addParagraph(Marker.stripMarkers(l), tag.format_start);
							} else {
								// E' presente un tag ma non è un marker: IMG o Symbol
								if (Picture.isFoto(curlyString)) {
									img = Picture.getByData(curlyString);
									if (img) {
										if (img.image.format_start.indexOf("<") !== -1) {
											const start = img.image.format_start;
											const splitStart = start.split(`src="`);
											const newStart = start.replace(splitStart[1], img.image.file);
											html += newStart + img.image.format_end;
										} else {
											html += `<p class="FOTO"><img src="${img.image.file}"/><p>`;
										}
									}
								}
							}
						} else {
							const mainTag = Marker.getTagByData(curlyContent[0].trim().slice(1, -1));
							if (mainTag) {
								if (this.closeTags.length > 0) {
									for (let i = 0; i < this.closeTags.length; i++) {
										html += this.closeTags[i];
									}
									this.closeTags = [];
								}
								l = l.replace(curlyContent[0], ``);
								curlyContent.splice(0, 1);
								html += this.addParagraph("", mainTag.format_start);
								curlyContent.forEach((c) => {
									let closePrevTag = "";
									if (this.closeTags.length > 1 && this.closeTags[1] === "</span>") {
										closePrevTag = this.closeTags[1];
										this.closeTags.splice(1, 1);
									}
									const spanTag = Marker.getTagByData(c.trim().slice(1, -1));
									if (spanTag) {
										l = l.replace(c, `${closePrevTag}<span ${spanTag.format_start}>`);
										this.closeTags.push("</span>");
									}
								});
								html += l;
							}
						}
					}
					//NON CI SONO TAG NELLA RIGA
					else html += "<br>" + l;
				}
			});
			if (this.closeTags.length > 0) {
				for (let i = 0; i < this.closeTags.length; i++) {
					html += this.closeTags[i];
				}
				this.closeTags = [];
			}
			this.dom.innerHTML = html;
		}

		private addParagraph(text: string, cls: string): string {
			this.closeTags.push("</p>");
			return `<p ${cls}>${text}`;
		}
	}
}
