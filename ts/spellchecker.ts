/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	declare const BJSpell: any;

	/**
	 * Classe di gestione del controllo Ortografico
	 */
	export class SpellChecker {
		private static lang: any;

		//Inizializzzione con caricamento asincrono del file vocabolario
		public static init(): Promise<void> {
			return new Promise((resolve) => {
				const lang = BJSpell("dictionary.js/it_IT.js", () => {
					Logger.log("Caricato Dizionario ITA");
					SpellChecker.lang = lang;
					resolve();
				});
			});
		}

		//Controlla la coerenza di una parola
		public static check(s: string): boolean {
			return SpellChecker.lang.check(s) as boolean;
		}

		//Suggerisce un numero N di parole alternative alla stringa data
		public static suggest(s: string, n: number): string[] {
			return SpellChecker.lang.suggest(s, n) as string[];
		}
	}
}
