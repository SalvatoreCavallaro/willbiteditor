// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe di gestione del resizer fra Panel e Preview
	 *
	 */
	export class SplitPanel {
		private x = 0;
		private leftWidth = 0;
		private resizer: HTMLElement;
		constructor(private leftSide: HTMLElement, private rightSide: HTMLElement) {
			this.resizer = document.getElementById("dragme") as HTMLElement;
			const mouseDownHandler = (e: MouseEvent) => {
				// recupera posizione mouse
				this.x = e.clientX;
				this.leftWidth = this.leftSide.getBoundingClientRect().width;
				document.addEventListener("mousemove", mouseMoveHandler);
				document.addEventListener("mouseup", mouseUpHandler);
			};

			const mouseMoveHandler = (e: MouseEvent) => {
				const dx = e.clientX - this.x;
				const newLeftWidth = ((this.leftWidth + dx) * 100) / (this.resizer.parentNode as HTMLElement).getBoundingClientRect().width;
				this.leftSide.style.width = `${newLeftWidth}%`;

				this.resizer.style.cursor = "col-resize";
				document.body.style.cursor = "col-resize";

				this.leftSide.style.userSelect = "none";
				this.leftSide.style.pointerEvents = "none";

				this.rightSide.style.userSelect = "none";
				this.rightSide.style.pointerEvents = "none";
			};

			const mouseUpHandler = () => {
				this.resizer.style.removeProperty("cursor");
				document.body.style.removeProperty("cursor");

				this.leftSide.style.removeProperty("user-select");
				this.leftSide.style.removeProperty("pointer-events");

				this.rightSide.style.removeProperty("user-select");
				this.rightSide.style.removeProperty("pointer-events");

				document.removeEventListener("mousemove", mouseMoveHandler);
				document.removeEventListener("mouseup", mouseUpHandler);
			};
			this.resizer.addEventListener("mousedown", mouseDownHandler);
		}
	}
}
