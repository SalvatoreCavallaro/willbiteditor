// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	interface Rec {
		label: string;
		tag: ITag | IImage;
		text: string;
	}

	/**
	 * Classe di gestione del contatore dei moduli
	 *
	 * E' stata riprodotta pedissequamente da una routine SQL
	 */
	export class ModuleCounter {
		private static pv_cnHeightFoto = 0.5;

		private static pv_cnHeightRest = 1.764;

		private static pv_baffoHeight = 2.469;

		private static pv_moduleCounter = 0.0;

		private static pv_heightCounter = 0.0;

		private static pv_height = 0;

		private static pv_width = 0;

		private static pv_grl_cuneo_necro = 0;

		public static compute(text: string, moduleParams: IModuleParams): number {
			const lines = text.split(Panel.lineBreakers).filter(Boolean);
			const records: string[] = [];
			let rec: Rec[] = [];
			lines.forEach((l) => {
				if (l.match(Marker.curlyHunterRegex)) records.push(l);
				else records[records.length - 1] += "\n" + l;
			});

			records.forEach((r) => {
				r = r.replace(/[\r\n]{2,}/g, "\n");
				const curlyContent = r.match(Marker.curlyHunterRegex);
				if (curlyContent && curlyContent.length > 0) {
					if (curlyContent.length === 1) {
						const curlyString = curlyContent[0].trim().slice(1, -1);
						const tag = Marker.getTagByData(curlyString);
						if (tag) rec.push({ tag: tag, label: tag.data, text: Marker.stripMarkers(r) });
						else {
							if (Picture.isFoto(curlyString)) {
								const img = Picture.getByData(curlyString);
								if (img) rec.push({ tag: img.image, label: "FOTO", text: img.image.data.split("|")[2].trim() });
							}
						}
					} else if (curlyContent.length >= 2) {
						const mainTag = Marker.getTagByData(curlyContent[0].trim().slice(1, -1));
						if (mainTag) {
							r = r.replace(curlyContent[0], ``);
							curlyContent.splice(0, 1);
							const extracted: Rec[] = [];
							for (let i = 0; i < curlyContent.length; i++) {
								const c = curlyContent[i];
								const curlyTag = Marker.getTagByData(curlyContent[i].trim().slice(1, -1));
								if (curlyTag) {
									const spanTagPos = r.indexOf(c);
									const nextTag = curlyContent[i + 1];
									if (nextTag) {
										const nextPos = r.indexOf(nextTag);
										extracted.push({ tag: curlyTag, label: curlyTag.data, text: r.substring(spanTagPos + c.length, nextPos) });
										r = r.substring(0, spanTagPos) + r.substring(nextPos);
									} else {
										extracted.push({ tag: curlyTag, label: curlyTag.data, text: r.substring(spanTagPos + c.length) });
										r = r.substring(0, spanTagPos);
									}
								}
							}
							rec.push({ tag: mainTag, label: mainTag.data, text: r });
							rec = [...rec, ...extracted];
						}
					}
				}
			});

			return ModuleCounter.moduleCounter(rec, moduleParams.width, moduleParams.height, moduleParams.cuneation);
		}

		/**
		 * static refreshModuleCounter
		 */
		private static refreshModuleCounter(testo: string, tag: ITag | IImage, tagLabel: string, mrc_prec: string | null): number {
			let lv_cn = 0;
			let lv_charsLength = 0;
			let lv_line = 0;
			let lv_firstLine = 0;
			let lv_lineCounter = 0;
			let lv_tempHeightCounter = 0;

			const lv_currentTag = tagLabel;
			const lv_previousTag = mrc_prec;
			if (lv_previousTag === "CO" && lv_currentTag === "ON") lv_cn = ModuleCounter.pv_baffoHeight;
			else if (lv_previousTag === "FOTO" && lv_currentTag === "NOME") lv_cn = ModuleCounter.pv_cnHeightFoto;
			else if (lv_previousTag === "NOME" && lv_currentTag === "CO") lv_cn = ModuleCounter.pv_cnHeightRest;
			else if (lv_previousTag === "NOME" && lv_currentTag === "T") lv_cn = ModuleCounter.pv_cnHeightRest;
			else if (lv_previousTag === "NOME" && lv_currentTag === "ON") lv_cn = ModuleCounter.pv_baffoHeight;
			else if (lv_previousTag === "SNO" && lv_currentTag === "T") lv_cn = ModuleCounter.pv_cnHeightFoto;
			else if (lv_previousTag === "SNO" && lv_currentTag === "CO") lv_cn = ModuleCounter.pv_cnHeightRest;
			else if (lv_previousTag === "T" && lv_currentTag === "ON") lv_cn = ModuleCounter.pv_baffoHeight;
			else if (lv_previousTag === "T" && lv_currentTag === "TT") lv_cn = ModuleCounter.pv_cnHeightRest;
			else if (lv_previousTag === "T" && lv_currentTag === "NOME") lv_cn = ModuleCounter.pv_cnHeightRest;
			else if (lv_previousTag === "T" && lv_currentTag === "FOTO") lv_cn = ModuleCounter.pv_cnHeightFoto;
			else if (lv_previousTag === "TN" && lv_currentTag === "ON") lv_cn = ModuleCounter.pv_baffoHeight;

			if (tagLabel !== "FOTO") {
				lv_charsLength = testo.length > 0 ? testo.length : 0;
				lv_charsLength = lv_charsLength * Number(tag.chars_size);
				lv_line = ModuleCounter.pv_width - Number(tag.right_indent) - Number(tag.left_indent);
				lv_firstLine = lv_line - Number(tag.first_line);

				lv_lineCounter = 0;

				// eslint-disable-next-line no-constant-condition
				while (true) {
					if (lv_charsLength < 0) break;
					if (lv_lineCounter === 0) lv_charsLength = lv_charsLength - lv_firstLine;
					else lv_charsLength = lv_charsLength - lv_line;
					lv_lineCounter = lv_lineCounter + 1;
				}
				lv_tempHeightCounter = Number(tag.space_before);
				lv_tempHeightCounter = lv_tempHeightCounter + Number(tag.space_after);
				lv_tempHeightCounter = lv_tempHeightCounter + Number(tag.leading) * lv_lineCounter;
			} else {
				lv_tempHeightCounter = Number(tag.space_before);
				lv_tempHeightCounter = lv_tempHeightCounter + Number(tag.space_after);
				lv_tempHeightCounter = lv_tempHeightCounter + Number(tag.height);
			}

			ModuleCounter.pv_heightCounter = ModuleCounter.pv_heightCounter + lv_tempHeightCounter;

			ModuleCounter.pv_heightCounter = ModuleCounter.pv_heightCounter + lv_cn;

			ModuleCounter.pv_moduleCounter = 0;

			if (ModuleCounter.pv_heightCounter <= ModuleCounter.pv_height) ModuleCounter.pv_moduleCounter = 1;
			else {
				lv_tempHeightCounter = ModuleCounter.pv_heightCounter;
				// eslint-disable-next-line no-constant-condition
				while (true) {
					if (lv_tempHeightCounter <= ModuleCounter.pv_height) {
						if (lv_tempHeightCounter >= 2) {
							ModuleCounter.pv_moduleCounter = ModuleCounter.pv_moduleCounter + 0.5;
							lv_tempHeightCounter = lv_tempHeightCounter - ModuleCounter.pv_height / 2;
							if (lv_tempHeightCounter >= 2) {
								ModuleCounter.pv_moduleCounter = ModuleCounter.pv_moduleCounter + 0.5;
							}
						}
						break;
					}
					lv_tempHeightCounter = lv_tempHeightCounter - ModuleCounter.pv_height;
					ModuleCounter.pv_moduleCounter = ModuleCounter.pv_moduleCounter + 1;
				}
			}

			ModuleCounter.pv_heightCounter = Math.trunc(ModuleCounter.pv_heightCounter * 10) / 10;

			return ModuleCounter.pv_moduleCounter;
		}

		private static moduleCounter(rec: Rec[], width: number, height: number, cuneo: number): number {
			ModuleCounter.pv_moduleCounter = 0;
			let lv_pos = 0;
			let lv_testo;
			let lv_testo_help: string | null = "";
			let lv_mrc_prec: string | null = null;
			ModuleCounter.pv_grl_cuneo_necro = cuneo;
			ModuleCounter.pv_width = width / 100;
			ModuleCounter.pv_height = height;
			ModuleCounter.pv_heightCounter = ModuleCounter.pv_grl_cuneo_necro / 10;

			rec.forEach((r) => {
				const pos = r.text.indexOf("\n"); //
				lv_pos = pos >= 0 ? pos : 0;
				if (lv_pos > 0) {
					lv_testo_help = r.text;
					// eslint-disable-next-line no-constant-condition
					while (true) {
						if (lv_testo_help === null) break;
						else {
							if (lv_pos === 0) {
								lv_testo = lv_testo_help;
								lv_testo_help = null;
							} else {
								lv_testo = lv_testo_help.substr(0, lv_pos);
								lv_testo_help = lv_testo_help.substr(lv_pos);
								const p = lv_testo_help.indexOf("\n");
								lv_pos = p >= 0 ? p : 0;
							}
							if (lv_testo.trim() !== "") ModuleCounter.refreshModuleCounter(lv_testo.trim(), r.tag, r.label, lv_mrc_prec);
							lv_mrc_prec = r.label;
						}
					}
				} else {
					if (r.text.trim() !== "") ModuleCounter.refreshModuleCounter(r.text.trim(), r.tag, r.label, lv_mrc_prec);
				}
				lv_mrc_prec = r.label;
			});
			return ModuleCounter.pv_moduleCounter;
		}
	}
}
