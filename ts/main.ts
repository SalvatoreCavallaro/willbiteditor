// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe wrapper principale
	 */
	export class Editor {
		private static ad: Ad;
		private static panel: Panel;
		private static preview: Preview;
		public static debug = false;

		private static basicUrl = "";

		/**
		 * Metodo pubblico di inizializzazione, viene invocato direttamente dentro l'HTML
		 */
		public static init(): void {
			/**
			 * Riferimenti al DOM
			 */
			const idEl = document.getElementById("annId");
			const preview = document.getElementById("preview");
			const panel = document.getElementById("editor");
			const spellcheck = document.getElementById("spellcheck");
			const spacecheck = document.getElementById("spacecheck");
			const revert = document.getElementById("revert");
			const save = document.getElementById("save");
			const exit = document.getElementById("exit");
			const savexit = document.getElementById("saveexit");
			const specialCharsBtn = document.getElementsByClassName("specialchars");
			const previewSizeInput = document.getElementById("previewsize") as HTMLInputElement;
			const datepicker = document.getElementById("datepickerbtn") as HTMLElement;

			/**
			 * Leggo e mostro la versione
			 */
			Editor.readVersion()
				.then((v) => {
					const vEl = document.getElementById("version");
					if (vEl) vEl.textContent = v;
				})
				.catch((e) => Logger.log(e + " Error reading version", "ERROR"));

			if (panel && preview)
				/**
				 * Inizializzazione Controllo ortografico
				 */
				SpellChecker.init().then(() => {
					const urlParams = new URLSearchParams(window.location.search);
					const query = urlParams.get("id");
					const split = query?.split(" ");
					Editor.basicUrl = split && split.length > 0 ? split[1] : "http://192.168.167.13/Editor/";
					const id = split && split.length > 0 ? split[0] : query;
					if (id && query)
						/**
						 * Leggo l'XML tramite i parametri contenuti nell'URL
						 */
						Loader.getXML(id, Editor.basicUrl).then((xml) => {
							Logger.log("Caricato Annuncio: " + id);

							if (idEl) idEl.innerHTML = id;

							//Creo Annuncio
							Editor.ad = new Ad(id, xml);

							//Carico Marker
							Editor.ad.getTags().forEach((t) => {
								new Marker(document.getElementById("markers-cont") as HTMLElement, t);
							});
							//Carico Drawers
							Editor.ad.getDrawers().forEach((d) => {
								new Drawer(document.getElementById("drawers-cont") as HTMLElement, d);
							});
							//Carico Simbols
							/* Editor.ad.getSimlols().forEach((s) => {
								new Simbol(document.getElementById("simbols-cont") as HTMLElement, s);
							}); */

							//Carico labelName
							const label = Editor.ad.getLabels();
							const labelCont = document.getElementById("label-cont") as HTMLElement;
							if (label) {
								new Label(labelCont, label);
							} else labelCont.style.display = "none";

							//Carico Immagini
							const imgNode = Editor.ad.getImages();
							if (imgNode.imgs.length > 0)
								imgNode.imgs.forEach((i) => {
									new Picture(document.getElementById("images-cont") as HTMLElement, i);
								});
							else {
								const imgCont = document.getElementById("images-cont");
								if (imgCont) imgCont.innerHTML = `<div class="w-full text-center p-2 text-gray-500">No Foto</div>`;
								Logger.log(`Nessuna immagine caricata`);
							}

							//Inizializzo Preview
							Editor.preview = new Preview(preview as HTMLElement);

							//Blocchiamo il pannello ridimensionabile in favore del dimensionamento puntuale
							//new SplitPanel(panel, preview);
							preview.style.width = previewSizeInput.value + "ch";

							previewSizeInput.addEventListener("change", () => {
								preview.style.width = previewSizeInput.value + "ch";
							});
							previewSizeInput.addEventListener("keyup", () => {
								preview.style.width = previewSizeInput.value + "ch";
							});
							const txt = Editor.ad.getMainText();

							//Associo la classe del set tipografico
							preview.classList.add("TS-" + Editor.ad.newspaper);

							//Inizializzo Panel
							Editor.panel = new Panel(panel, txt);
							Editor.preview.update();
							Editor.updateCounters();
							//Se Necro attivo conteggio moduli
							if (Editor.ad.isNecroModule()) Editor.updataModule();

							//Metodo di update dei dati ad ogni aggiornamento del Panel
							const onUpdate = () => {
								Editor.panel.update();
								Editor.preview.update();
								Editor.updateCounters();
								if (Editor.ad.isNecroModule()) Editor.updataModule();
							};

							//Observer per variazioni sul Panel
							new TextObserver(panel, onUpdate);

							document.addEventListener("keydown", Editor.keyBindings);

							//Inizializzazione pulsanti
							for (let i = 0; i < specialCharsBtn.length; i++) {
								const scBtn = specialCharsBtn.item(i);
								scBtn?.addEventListener("click", () => Editor.addSpecialChar(scBtn.getAttribute("data-char") as string));
							}

							if (spellcheck && preview) {
								spellcheck.addEventListener("click", () => Editor.panel.spellcheck());
								new Shortcut(spellcheck, () => Editor.panel.spellcheck(), "KeyC");
							}
							if (spacecheck && preview) {
								spacecheck.addEventListener("click", () => Editor.preview.spacecheck());
								new Shortcut(spacecheck, () => Editor.preview.spacecheck(), "KeyB");
							}
							if (revert) {
								new Shortcut(revert, () => Editor.panel.reset(), "KeyR");
								revert.addEventListener("click", () => Editor.panel.reset());
							}
							if (save) {
								save.addEventListener("click", () => Editor.save());
								new Shortcut(save, () => Editor.save(), "KeyS");
							}
							if (exit) {
								exit.addEventListener("click", () => Editor.exit());
								new Shortcut(exit, () => Editor.exit(), "KeyX");
							}

							if (datepicker) {
								datepicker.addEventListener("click", () => Editor.addDatePlusOne());
								new Shortcut(datepicker, () => Editor.addDatePlusOne(), "KeyD");
							}

							if (savexit) {
								const handle = () => {
									const id = Editor.ad.getId();
									const saveUrl = Editor.basicUrl + "savetext.php";
									const exitUrl = Editor.basicUrl + "closetext.php?id=" + id;
									const text = Editor.panel.getCurrentText();
									const idMsg = Editor.ad.isNecroModule() ? "id=" + id + "|" + ModuleCounter.compute(text, Editor.ad.getModuleParams()).toString() : "id=" + id;
									const textMsg = "&text=" + encodeURIComponent(text);
									const drawers = Drawer.getAll();
									let drawersMsg = "";
									for (let i = 1; i <= drawers.length; i++) {
										const d = drawers[i - 1];
										const dText = d.getText();
										drawersMsg += "&drawer_" + i.toString() + "=";
										drawersMsg += dText !== null ? dText : "";
									}
									const xhr = new XMLHttpRequest();
									xhr.open("POST", saveUrl, true);

									//Send the proper header information along with the request
									xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

									xhr.onreadystatechange = function () {
										// Call a function when the state changes.
										if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
											const httpcall = new XMLHttpRequest();

											httpcall.onload = () => window.close();

											httpcall.onerror = () => {
												Logger.log("Error while exiting.");
											};

											httpcall.open("GET", exitUrl);
											httpcall.send();
										}
									};
									xhr.onerror = () => {
										Logger.log("Error while Saving.");
									};
									xhr.send(idMsg + textMsg + drawersMsg);
								};
								savexit.addEventListener("click", () => handle());
								new Shortcut(savexit, () => handle(), "KeyQ");
							}
						});
				});
		}

		/**
		 * Metodo statico per l'uscita dall'Editor
		 */
		private static exit() {
			const id = Editor.ad.getId();
			const url = Editor.basicUrl + "closetext.php?id=" + id;

			const xhr = new XMLHttpRequest();

			xhr.onload = () => window.close();

			xhr.onerror = () => {
				Logger.log("Error while exiting.");
			};

			xhr.open("GET", url);
			xhr.send();
		}

		/**
		 * Metodo statico per il salvataggio dell'annuncio
		 */
		private static save() {
			const id = Editor.ad.getId();
			const url = Editor.basicUrl + "savetext.php";
			const text = Editor.panel.getCurrentText();
			const idMsg = Editor.ad.isNecroModule() ? "id=" + id + "|" + ModuleCounter.compute(text, Editor.ad.getModuleParams()).toString() : "id=" + id;
			const textMsg = "&text=" + encodeURIComponent(Panel.strip(text));
			const drawers = Drawer.getAll();
			let drawersMsg = "";
			for (let i = 1; i <= drawers.length; i++) {
				const d = drawers[i - 1];
				const dText = d.getText();
				drawersMsg += "&drawer_" + i.toString() + "=";
				drawersMsg += dText !== null ? dText : "";
			}
			const xhr = new XMLHttpRequest();
			xhr.open("POST", url, true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

			xhr.onreadystatechange = function () {
				if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
					console.log("salvataggio OK", this.response);
				}
			};
			xhr.onerror = () => {
				Logger.log("Error while Saving.");
			};
			xhr.send(idMsg + textMsg + drawersMsg);
			Logger.log("Annuncio salvato");
		}

		// Aggiorna l'icona del controllo ortografico (ON/OFF)
		public static updateSpellCheckIcon(checked: boolean): void {
			const sci = document.getElementById("spellCheckIcon");
			if (sci) checked ? sci.classList.remove("hidden") : sci.classList.add("hidden");
		}
		// Aggiorna l'icona del controllo ortografico (ON/OFF)
		public static updateSpaceCheckIcon(checked: boolean): void {
			const sci = document.getElementById("spaceCheckIcon");
			if (sci) checked ? sci.classList.remove("hidden") : sci.classList.add("hidden");
		}

		/**
		 * Metodo di gestione dell'interazione con la tastiera
		 * @param e Evento tastiera
		 */
		private static keyBindings(e: KeyboardEvent) {
			// Tasto ALT, evidenziamo i componenti che hanno una shortcut
			if (e.altKey && !e.shiftKey) {
				e.preventDefault();
				if (!Shortcut.isLighting) {
					Shortcut.highlight();
					const keyup = (e: KeyboardEvent) => {
						Shortcut.call(e.code);
						if (!e.altKey) {
							Shortcut.lightOff();
							document.removeEventListener("keyup", keyup);
						}
					};
					document.addEventListener("keyup", keyup);
				}
			} else {
				const marker = Marker.getMarkerByShortcut(e.key);
				// Marker da tasto Fn
				if (marker) {
					e.preventDefault();
					Editor.addMarker(marker.tag);
				} else {
					const target = Panel.getCursorElement();
					const inside = Panel.getDOM().contains(target);
					// Click interno al pannello
					if (target && inside) {
						//controllo su inserimento linea vuota
						if (e.code === "Enter" && target.previousSibling !== null && target.previousSibling.textContent !== null && target.previousSibling.textContent === "") e.preventDefault();
						else {
							const lastKnownPosition = Panel.getLastCaretPosition();
							if (lastKnownPosition !== null && target.textContent) {
								const caretPosWithin = Panel.getCaretCharacterOffsetWithin(target);
								const fromStartToCaret = target.textContent.substring(0, caretPosWithin) || "";
								const lasttargetTxt = fromStartToCaret.substr(fromStartToCaret.lastIndexOf("{"));
								const lastOpenMarkerFromStart = fromStartToCaret.lastIndexOf("{");
								const lastCloseMarkerFromStart = fromStartToCaret.lastIndexOf("}");
								const fromCaretToEnd = target.textContent.substring(caretPosWithin);
								const firstOpenMarkerToEnd = fromCaretToEnd.indexOf("{");
								const firstCloseMarkerToEnd = fromCaretToEnd.indexOf("}");
								const inCurly =
									lastOpenMarkerFromStart !== -1 &&
									(lastCloseMarkerFromStart === -1 || lastOpenMarkerFromStart > lastCloseMarkerFromStart) &&
									firstCloseMarkerToEnd !== -1 &&
									(firstOpenMarkerToEnd === -1 || firstOpenMarkerToEnd > firstCloseMarkerToEnd);

								if (inCurly) {
									const insideCurly = target.textContent.substring(lastOpenMarkerFromStart + 1, caretPosWithin + firstCloseMarkerToEnd);
									const insideMarker = inCurly && (Marker.getTagByData(insideCurly) || Picture.isFoto(insideCurly));
									// siamo dentro il tag di un Marker o di una foto, consento solo lo spostamento tramite frecce
									if (insideMarker && e.code !== "ArrowRight" && e.code !== "ArrowLeft" && e.code !== "ArrowDown" && e.code !== "ArrowUp") e.preventDefault();
								}

								//Cancellazione dicotomica dei Marker
								else if (e.code === "Backspace" && lasttargetTxt[lasttargetTxt.length - 1] === "}") {
									const insideCurly = target.textContent.substring(lastOpenMarkerFromStart + 1, lastCloseMarkerFromStart);
									const targetMarker = Marker.getTagByData(insideCurly);
									if (targetMarker) {
										e.preventDefault();
										target.textContent = target.textContent.substring(0, lastOpenMarkerFromStart) + target.textContent.substring(lastCloseMarkerFromStart + 1);
										Panel.caretAt(lastKnownPosition - (lastCloseMarkerFromStart - lastOpenMarkerFromStart + 1));
									}
								} else if (lasttargetTxt !== "" && e.code === "Space") {
									const active = Marker.getActiveMarker(target);
									if (active) {
										const maxW = Marker.hasMaxWords(active);
										//Controllo su massimo numero di parole
										if (maxW && wordCounter(Marker.stripMarkers(lasttargetTxt)) === maxW) {
											Logger.log("Tag " + active.data + " max word reached --> " + maxW, "ERROR");
											e.preventDefault();
										}
									}
								}
								//Durante la digitazione se precedentemente attivo viene resettato il controllo ortografico
								if (Panel.spellChecked) Editor.resetSpellCheck();
								if (Preview.spaceChecked) Editor.resetSpaceCheck();
							}
						}
					}
				}
			}
			return false;
		}

		private static addDatePlusOne() {
			const date = new Date();
			const months = ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"];
			// add a day
			date.setDate(date.getDate() + 1);
			const day = date.getDate();
			const month = months[date.getMonth()];
			const year = date.getFullYear();
			const dateString = `${day} ${month} ${year}`;
			const pos = Panel.getLastCaretPosition();
			const dom = Panel.getDOM();
			if (pos !== null) {
				const el = Panel.getCursorElement();
				if (el && dom.contains(el)) {
					const posWithin = Panel.getCaretCharacterOffsetWithin(el);
					if (posWithin !== null && el.textContent !== null) {
						el.textContent = el.textContent.substr(0, posWithin) + dateString + el.textContent.substr(posWithin);
						Panel.addToCaretPosition(dateString.length);
						Panel.caretWithinAt(el, posWithin + dateString.length);
					}
				} else {
					if (Panel.LCE.element !== null && Panel.LCE.offset !== null && Panel.LCE.element.textContent !== null) {
						Panel.LCE.element.textContent = Panel.LCE.element.textContent.substr(0, Panel.LCE.offset) + dateString + Panel.LCE.element.textContent.substr(Panel.LCE.offset);
						Panel.addToCaretPosition(dateString.length);
						Panel.caretWithinAt(Panel.LCE.element, Panel.LCE.offset + dateString.length);
					} else {
						dom.focus();
						Panel.caretAt(pos);
						Editor.panel.insertTextAtCaret(dateString);
					}
				}
			} else {
				Editor.panel.appendDatePlusOne(dateString);
				Panel.caretAt(dateString.length);
			}
			Panel.updateCaretPosition();
		}

		//Reset dek controllo ortografico
		public static resetSpellCheck(): void {
			Editor.panel.uncheckSpell();
			const pos = Panel.getLastCaretPosition();
			if (pos) Panel.caretAt(pos);
		}
		//Reset dek controllo ortografico
		public static resetSpaceCheck(): void {
			Editor.preview.resetSpace();
			const pos = Panel.getLastCaretPosition();
			if (pos) Panel.caretAt(pos);
		}

		/**
		 * Metodo per caricare da file la versione attuale
		 * @returns Promise contenente la stringa di versione
		 */
		private static readVersion(): Promise<string> {
			return new Promise((res, rej) => {
				const xhr = new XMLHttpRequest();

				xhr.onload = () => {
					if (xhr.response) {
						const rows = xhr.response.split("\n") as string[];
						res(rows[rows.length - 1].substring(0, 20));
					}
				};

				xhr.onerror = () => {
					Logger.log("Error while getting XML.");
					rej("Error while getting XML.");
				};

				xhr.open("GET", "version.txt");
				xhr.send();
			});
		}

		/**
		 * Metodo statico di aggiornamento dei contatori di caratteri e parole
		 */
		private static updateCounters() {
			const text = stripBlankLines(Marker.stripMarkers(Editor.panel.getCurrentText()));
			const result = textCounter(text);
			const wrdsEl = document.getElementById("words");
			const charsEl = document.getElementById("chars");
			if (wrdsEl) wrdsEl.textContent = result.words.toString();
			if (charsEl) charsEl.textContent = result.characters.toString();
		}

		/**
		 * Metodo statico di aggiornamento del contatore di moduli
		 */
		private static updataModule() {
			const txt = Editor.panel.getCurrentText();
			const moduleParams = Editor.ad.getModuleParams();
			const n = ModuleCounter.compute(txt, moduleParams);
			const mdlsEl = document.getElementById("modules");
			if (mdlsEl) mdlsEl.textContent = n.toString();
		}

		/**
		 * Metodo statico che aggiunge un Marker al panel nella posizione opportuna
		 */
		public static addMarker(tag: ITag): void {
			const pos = Panel.getLastCaretPosition();
			const dom = Panel.getDOM();
			const addtext = tag.text && tag.text !== "" ? `{${tag.data}}${tag.text.replace(/\n/g, "<br>")}` : `{${tag.data}}`;
			const addOffset = tag.text && tag.text !== "" ? tag.data.length + tag.text.length + 2 : tag.data.length + 2;
			if (pos !== null) {
				const selected = Panel.getSelection();
				if (selected && dom.contains(selected.elem)) {
					Panel.replaceSelectedText(addtext);
					Panel.addToCaretPosition(addOffset - selected.text.length);
				} else {
					const el = Panel.getCursorElement();
					if (el && dom.contains(el)) {
						//DENTRO PANNELLO => ShortCUT
						const posWithin = Panel.getCaretCharacterOffsetWithin(el);
						if (posWithin !== null && el.innerHTML !== null) {
							el.innerHTML = el.innerHTML.substr(0, posWithin) + addtext + el.innerHTML.substr(posWithin);
							Panel.addToCaretPosition(addOffset);
							Panel.caretWithinAt(el, posWithin + addOffset);
						}
					} else {
						//FUORI PANNELLO => Button
						if (Panel.LCE.element !== null && Panel.LCE.offset !== null && Panel.LCE.element.innerHTML !== null) {
							Panel.LCE.element.innerHTML = Panel.LCE.element.innerHTML.substr(0, Panel.LCE.offset) + addtext + Panel.LCE.element.innerHTML.substr(Panel.LCE.offset);
							Panel.addToCaretPosition(addOffset);
							Panel.caretWithinAt(Panel.LCE.element, Panel.LCE.offset + addOffset);
						} else {
							dom.focus();
							Panel.caretAt(pos);
							Editor.panel.insertTextAtCaret(addtext);
						}
					}
				}
			} else {
				Editor.panel.appendTag(tag);
				Panel.caretAt(addOffset);
			}
		}

		/**
		 * Metodo statico che aggiunge una Foto al panel nella posizione opportuna
		 */
		public static addPhoto(image: IImage): void {
			const pos = Panel.getLastCaretPosition();
			const dom = Panel.getDOM();
			const addtext = `{${image.data}}`;
			const div = document.createElement("div");
			if (pos !== null) {
				const selected = Panel.getSelection();
				if (selected && dom.contains(selected.elem)) {
					Panel.replaceSelectedText(addtext);
					Panel.addToCaretPosition(addtext.length - selected.text.length);
				} else {
					const el = Panel.getCursorElement();
					if (el && dom.contains(el)) {
						const posWithin = Panel.getCaretCharacterOffsetWithin(el);
						if (posWithin !== null && el.textContent !== null) {
							div.textContent = el.textContent.substr(posWithin);
							el.textContent = el.textContent.substr(0, posWithin) + addtext;
							div.appendChild(document.createElement("br"));
							Panel.insertAfter(div, el);
							Panel.setCaretToStartWithin(div);
						}
					} else {
						if (Panel.LCE.element !== null && Panel.LCE.offset !== null && Panel.LCE.element.textContent !== null) {
							div.textContent = Panel.LCE.element.textContent.substr(Panel.LCE.offset);
							Panel.LCE.element.textContent = Panel.LCE.element.textContent.substr(0, Panel.LCE.offset) + addtext;
							div.appendChild(document.createElement("br"));
							Panel.insertAfter(div, Panel.LCE.element);
							Panel.setCaretToStartWithin(div);
						} else {
							dom.focus();
							Panel.caretAt(pos);
							Editor.panel.insertTextAtCaret(addtext);
							div.appendChild(document.createElement("br"));
							dom.appendChild(div);
							Panel.setCaretToEnd();
						}
					}
				}
			} else {
				Editor.panel.appendPhoto(image);
				div.appendChild(document.createElement("br"));
				dom.appendChild(div);
				Panel.setCaretToEnd();
			}
			Panel.LCE.element = div;
			Panel.LCE.offset = 0;
		}

		/**
		 * Metodo statico che aggiunge un Simbol al panel nella posizione opportuna
		 */
		public static addSimbol(simb: { data: string; label: string }): void {
			const pos = Panel.getLastCaretPosition();
			if (pos !== null) {
				Panel.caretAt(pos);
				const target = Panel.getCursorElement();
				const inside = Panel.getDOM().contains(target);
				if (target && inside) {
					Editor.panel.insertTextAtCaret(`{${simb.data}}`);
					Panel.addToCaretPosition(simb.data.length + 2);
				}
			} else {
				const last = Panel.getLastTextElement();
				if (last) {
					Editor.panel.appendTextToElem(`{${simb.data}}`, last);
					Panel.setCaretToEnd();
				}
			}
		}

		/**
		 * Metodo statico che aggiunge un Simbol al panel nella posizione opportuna
		 */
		public static addLabel(label: string): void {
			const pos = Panel.getLastCaretPosition();
			if (pos !== null) {
				Panel.caretAt(pos);
				const target = Panel.getCursorElement();
				const inside = Panel.getDOM().contains(target);
				if (target && inside) {
					Editor.panel.insertTextAtCaret(`${label}`);
					Panel.addToCaretPosition(label.length + 2);
				}
			} else {
				const last = Panel.getLastTextElement();
				if (last) {
					Editor.panel.appendTextToElem(`${label}`, last);
					Panel.setCaretToEnd();
				}
			}
		}

		/**
		 * Metodo statico che aggiunge un Simbol al panel nella posizione opportuna
		 */
		public static addSpecialChar(charCode: string): void {
			const char = String.fromCharCode(Number(charCode));
			const pos = Panel.getLastCaretPosition();
			if (pos !== null) {
				Panel.caretAt(pos);
				const target = Panel.getCursorElement();
				const inside = Panel.getDOM().contains(target);
				if (target && inside) {
					Editor.panel.insertTextAtCaret(`${char}`);
					Panel.addToCaretPosition(1);
				}
			} else {
				const last = Panel.getLastTextElement();
				if (last) {
					Editor.panel.appendTextToElem(`${char}`, last);
					Panel.setCaretToEnd();
				}
			}
		}

		/**
		 * Metodo statico che aggiunge il testo di un Drawer al panel nella posizione opportuna
		 */
		public static addDrawerText(text: string): void {
			const pos = Panel.getLastCaretPosition();
			if (pos !== null) {
				Panel.caretAt(pos);
				const target = Panel.getCursorElement();
				const inside = Panel.getDOM().contains(target);
				if (target && inside) {
					Editor.panel.insertTextAtCaret(text);
					Panel.addToCaretPosition(text.length + 1);
				}
			} else {
				const last = Panel.getLastTextElement();
				if (last) {
					Editor.panel.appendTextToElem(text, last);
					Panel.setCaretToEnd();
				}
			}
		}

		// restituisce il testo attuale dentro il panel
		public static getCurrentText(): string {
			return Editor.panel.getCurrentText();
		}
	}
}
