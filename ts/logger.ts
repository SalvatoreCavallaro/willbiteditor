// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe di gestione dei log su rullo
	 */
	export class Logger {
		public static log(msg: string, type?: "ERROR"): void {
			console.log(msg);
			const lg = document.getElementById("logger");
			if (lg) {
				if (type && type === "ERROR") msg = `<span class="text-red-300">${type + ": " + msg}</span>`;
				lg.innerHTML += "> " + msg + "<br>";
				lg.scrollTop = lg.scrollHeight;
			}
		}
	}
}
