// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	/**
	 * Classe di caricamento e parsing dell'XML
	 */
	export class Loader {
		private static composeURL(id: string, url: string): string {
			return Editor.debug ? "./EsempioOnoranza.xml" : url + "loadtext.php?id=" + id;
		}

		public static getXML(id: string, url: string): Promise<Document> {
			return new Promise((res, rej) => {
				const xhr = new XMLHttpRequest();

				xhr.onload = () => {
					if (xhr.responseXML) {
						res(xhr.responseXML);
					}
				};

				xhr.onerror = () => {
					Logger.log("Error while getting XML.");
					rej("Error while getting XML.");
				};

				xhr.open("GET", Loader.composeURL(id, url));
				xhr.responseType = "document";
				xhr.overrideMimeType("text/xml");
				xhr.send();
			});
		}
	}
}
