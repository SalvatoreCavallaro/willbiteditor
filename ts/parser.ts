/* eslint-disable @typescript-eslint/no-unused-vars */
namespace WB {
	/**
	 * Semplice wrapper di gestione al parser Javascript nativo
	 */
	export class XMLParser {
		public static fromString(xml: string): Document {
			const p = new DOMParser();
			const xmlDoc = p.parseFromString(xml, "text/xml");
			return xmlDoc;
		}
	}
}
