// eslint-disable-next-line @typescript-eslint/no-unused-vars
namespace WB {
	interface PrintablegObj {
		label: string;
		data: string;
		format_start: string;
		format_end: string;
		flag_rif: string;
		leading: string;
		space_before: string;
		space_after: string;
		right_indent: string;
		left_indent: string;
		first_line: string;
		chars_size: string;
		height: string;
	}
	export interface ITag extends PrintablegObj {
		text: string;
		maxwords: string;
	}

	export interface IImage extends PrintablegObj {
		tag: string;
		fteid: string;
		file: string;
	}

	export interface IModuleParams {
		width: number;
		height: number;
		cuneation: number;
	}

	/**
	 * Classe Annuncio ricalca fedelmente la semantica dell'XML
	 *
	 * Il significato dei parametri è il seguente:
	 **
	 **   eserc_id: anno di due cifre
	 **   soc_id: società
	 **   uop_id : unità operativa
	 **   fil_id : filiale
	 **   nmg_id:  numeratore
	 **   id     : identificativo dell’annuncio
	 **   utente
	 **   tst_id : testata giornalistica //TODO: identificativo testata
	 **   edz_id: edizione
	 **   rbra_seq_id: identificativo rubrica
	 **   atipa_id: tipo di testo (valido solo per gli economici)
	 **   descrizione della agenzia, può essere vuoto
	 **   'CN' : indica che è una necrologia CEN indica che è un economico
	 **   1= la foto è obbligatoria all’interno del testo
	 **   1= la necrologia è tabellare quindi vanno calcolati i moduli
	 **   Base del modulo  ( 3485 )
	 **   Altezza modulo  (20)
	 **   Cuneazione (filetto) (21)
	 **
	 **   Gli ultimi 3 dati sono necessari per il calcolo dei moduli e dipendono dalla testata.
	 */
	export class Ad {
		private year;
		private company;
		private operativeUnit;
		private filial;
		private numerator;
		private id;
		private user;
		public newspaper;
		private edition;
		private rubric;
		private atipa_id;
		private description;
		private type;
		private picMandatory;
		private isNecTab;
		private moduleBase;
		private moduleHeight;
		private cuneation;
		private tagrifs: Element | null = null;
		private tagaggs: Element | null = null;
		private error: {
			number: string | null;
			description: string | null;
		} | null = null;
		private tags: ITag[] = [];
		private simbols: {
			label: string;
			data: string;
		}[] = [];
		private labelName = "";
		private isframed = false;
		private drawers: string[] = [];
		private parameters = "";
		private maintext = "";
		private images: IImage[] = [];
		private imageType = "";

		constructor(private original: string, private xml: Document) {
			const o = original.split("|");
			this.year = o[0];
			this.company = o[1];
			this.operativeUnit = o[2];
			this.filial = o[3];
			this.numerator = o[4];
			this.id = o[5];
			this.user = o[6];
			this.newspaper = o[7]; //TODO: identificativo testata
			this.edition = o[8];
			this.rubric = o[9];
			this.atipa_id = o[10];
			this.description = o[11];
			this.type = o[12];
			this.picMandatory = o[13] === "1";
			this.isNecTab = o[14] === "1";
			this.moduleBase = o[15];
			this.moduleHeight = o[16];
			this.cuneation = o[17];
			this.feedXml();
		}

		private feedXml() {
			console.log(this.xml);
			const errNode = this.xml.getElementsByTagName("error").item(0);
			if (errNode) {
				this.error = {
					number: errNode.getAttribute("number"),
					description: errNode.getAttribute("description"),
				};
				if (this.error.number !== "0" && this.error.description !== "") Logger.log("Errore feedXml: " + this.error.description + " (code: " + this.error.number + ")");
			}
			const labels = this.xml.getElementsByTagName("labels").item(0)?.getElementsByTagName("label");
			if (labels) {
				for (const l of labels) {
					this.labelName = l.getAttribute("data") || "";
				}
			}

			const tags = this.xml.getElementsByTagName("tags").item(0)?.getElementsByTagName("tag");
			if (tags)
				for (const t of tags) {
					const label = t.getAttribute("label") || "";
					const data = t.getAttribute("data") || "";
					const text = t.getAttribute("text")?.replace(/\r|\n/g, "\r\n") || "";
					const maxwords = t.getAttribute("maxwords") || "";
					const format_start = t.getAttribute("format_start") || "";
					const format_end = t.getAttribute("format_end") || "";
					const flag_rif = t.getAttribute("flag_rif") || "";
					const leading = t.getAttribute("leading") || "";
					const space_before = t.getAttribute("space_before") || "";
					const space_after = t.getAttribute("space_after") || "";
					const right_indent = t.getAttribute("right_indent") || "";
					const left_indent = t.getAttribute("left_indent") || "";
					const first_line = t.getAttribute("first_line") || "";
					const chars_size = t.getAttribute("chars_size") || "";
					const height = t.getAttribute("height") || "";
					this.tags.push({
						label,
						data,
						text,
						maxwords,
						format_start,
						format_end,
						flag_rif,
						leading,
						space_before,
						space_after,
						right_indent,
						left_indent,
						first_line,
						chars_size,
						height,
					});
				}
			this.tagrifs = this.xml.getElementsByTagName("tagrifs")?.item(0);
			this.tagaggs = this.xml.getElementsByTagName("tagaggs")?.item(0);
			const symbols = this.xml.getElementsByTagName("symbols").item(0)?.getElementsByTagName("symbol");
			if (symbols) {
				for (const s of symbols) {
					const label = s.getAttribute("label") || "";
					const data = s.getAttribute("data") || "";
					this.simbols.push({ label, data });
				}
			}

			this.isframed = this.xml.getElementsByTagName("frame")?.item(0)?.getAttribute("isframed") === "true";
			const drawers = this.xml.getElementsByTagName("drawers")?.item(0)?.getElementsByTagName("drawer");
			if (drawers) {
				for (const d of drawers) {
					const value = d.childNodes[0].nodeValue;
					if (value) this.drawers.push(value);
					else this.drawers.push("EMPTY");
				}
			}
			const textruns = this.xml.getElementsByTagName("textruns").item(0);
			if (textruns) {
				this.parameters = textruns.getElementsByTagName("parameters")?.item(0)?.childNodes[0]?.nodeValue || "";
				this.maintext = textruns.getElementsByTagName("maintext")?.item(0)?.childNodes[0]?.nodeValue || "";
			}
			const images = this.xml.getElementsByTagName("immagini").item(0)?.getElementsByTagName("immagine");
			if (images) {
				this.imageType = this.xml.getElementsByTagName("immagini").item(0)?.getAttribute("tipo") || "";
				for (const i of images) {
					const label = i.getAttribute("label") || "";
					const data = i.getAttribute("data") || "";
					const tag = i.getAttribute("tag") || "";
					const fteid = i.getAttribute("fteid") || "";
					const file = i.getAttribute("file") || "";
					const format_start = i.getAttribute("format_start") || "";
					const format_end = i.getAttribute("format_end") || "";
					const flag_rif = i.getAttribute("flag_rif") || "";
					const leading = i.getAttribute("leading") || "";
					const space_before = i.getAttribute("space_before") || "";
					const space_after = i.getAttribute("space_after") || "";
					const right_indent = i.getAttribute("right_indent") || "";
					const left_indent = i.getAttribute("left_indent") || "";
					const first_line = i.getAttribute("first_line") || "";
					const chars_size = i.getAttribute("chars_size") || "";
					const height = i.getAttribute("height") || "";
					this.images.push({
						label,
						data,
						tag,
						fteid,
						file,
						format_start,
						format_end,
						flag_rif,
						leading,
						space_before,
						space_after,
						right_indent,
						left_indent,
						first_line,
						chars_size,
						height,
					});
				}
			}
		}

		public getModuleParams(): IModuleParams {
			return { width: Number(this.moduleBase) || 0, height: Number(this.moduleHeight) || 0, cuneation: Number(this.cuneation) || 0 };
		}

		public getType(): string {
			return this.type;
		}

		public isNecroModule(): boolean {
			return this.type === "CN" && this.isNecTab;
		}

		public getTags(): ITag[] {
			return this.tags;
		}

		public getMainText(): string {
			return this.maintext;
		}

		public getDrawers(): string[] {
			return this.drawers;
		}

		public getSimlols(): { data: string; label: string }[] {
			return this.simbols;
		}

		public getLabels(): string {
			return this.labelName;
		}

		public getId(): string {
			if (this.isNecroModule()) return this.original;
			else {
				const o = this.original.split("|");
				return o.slice(0, 13).join("|");
			}
		}

		public getImages(): {
			type: string;
			imgs: IImage[];
		} {
			return { type: this.imageType, imgs: this.images };
		}
	}
}
